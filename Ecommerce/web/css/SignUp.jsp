<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    
    <jsp:include page="Header.jsp" />
    	
<center>
	<section id="form"><!--form-->
            <center>
		<div class="container">
			<div class="row" >
                            
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
                                            <center>
						<h2>New User Signup!</h2>
                                                <form action="SignUpServlet" method="post" style="float:middle">
                                                    <input type="text" name="firstName" id="fnamee" placeholder="First Name" onfocus="focusFunction1()" onblur="blurFunction1()" required=""/>
                                                    <div id="fnameediv">  </div>
                                                    
                                                    <input type="text" name="lastName" id="lnamee" placeholder="Last Name" onfocus="focusFunction2()" onblur="blurFunction2()" required=""/>
                                                    <div id="lnameediv"></div>
                                                    
                                                    <input type="email" name="email" id="emaile" placeholder="Email Address" onfocus="focusFunction3()" onblur="blurFunction3()" required=""/>
                                                    <div id="emailediv"></div>
                                                    
                                                    <input type="password" name="password" id="passworde" placeholder="Password" onfocus="focusFunction4()" onblur="blurFunction4()" required=""/>
                                                    <div id="passwordediv"></div>
                                                    
                                                    <input type="text" name="job" id="jobe" placeholder="Job" onfocus="focusFunction5()" onblur="blurFunction5()" required=""/>
                                                    <div id="jobediv"></div>
                                                    
                                                    <input type="text" name="country" id="countrye" placeholder="Country" onfocus="focusFunction6()" onblur="blurFunction6()" required=""/>
                                                    <div id="countryediv"></div>
                                                    
                                                    <input type="text" name="city" id="citye" placeholder="City" onfocus="focusFunction7()" onblur="blurFunction7()" required=""/>
                                                    <div id="cityediv"></div>
                                                    
                                                    <input type="text" name="street" id="streete" placeholder="Street" onfocus="focusFunction8()" onblur="blurFunction8()" required=""/>
                                                    <div id="streetediv"></div>
                                                    
                                                    <input type="text" name="balance" id="balancee" placeholder="Balance" onfocus="focusFunction9()" onblur="blurFunction9()" required=""/>
                                                    <div id="balanceediv"></div>
                                                    
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
                                            </center>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
            </center>
	</section><!--/form-->
</center>
	
        
        <jsp:include page="Footer.jsp" />
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    
    <script type="text/javascript">
        function focusFunction1() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("fnamee").style.background = "yellow";
        }

function blurFunction1() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("fnamee").style.background = "red";
    var i1=document.getElementById("fnamee").value;
    document.getElementById("fnameediv").innerHTML = "";
    if(i1 == '')
    {
    var string1="You Need To Enter Your First Name";
    document.getElementById("fnameediv").innerHTML = string1 ;

            alert("Please Enter Your First Name!");   
   
    }

    
}


 function focusFunction2() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("lnamee").style.background = "yellow";
        }

function blurFunction2() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("lnamee").style.background = "red";
    var i2=document.getElementById("lnamee").value;
    document.getElementById("lnameediv").innerHTML ="";
    if(i2 == '')
    {
    var string2="You Need To Enter Your Last Name";
    document.getElementById("lnameediv").innerHTML = string2 ;

            alert("Please Enter Your Last Name!");   
   
    }

    
}

function focusFunction3() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("emaile").style.background = "yellow";
        }

function blurFunction3() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("emaile").style.background = "red";
    var i3=document.getElementById("emaile").value;
    document.getElementById("emailediv").innerHTML ="";
    if(i3 == '')
    {
    var string3="You Need To Enter Your Email";
    document.getElementById("emailediv").innerHTML = string3 ;

            alert("Please Enter Your Email!");   
   
    }

    
}


function focusFunction4() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("passworde").style.background = "yellow";
        }

function blurFunction4() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("passworde").style.background = "red";
    var i4=document.getElementById("passworde").value;
    document.getElementById("passwordediv").innerHTML ="";
    if(i4 == '')
    {
    var string4="You Need To Enter Your Password";
    document.getElementById("passwordediv").innerHTML = string4 ;

            alert("Please Enter Your Password!");   
   
    }

    
}


function focusFunction5() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("jobe").style.background = "yellow";
        }

function blurFunction5() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("jobe").style.background = "red";
    var i5=document.getElementById("jobe").value;
    document.getElementById("jobediv").innerHTML ="";
    if(i5 == '')
    {
    var string5="You Need To Enter Your Job";
    document.getElementById("jobediv").innerHTML = string5 ;

            alert("Please Enter Your Job!");   
   
    }

    
}

function focusFunction6() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("countrye").style.background = "yellow";
        }

function blurFunction6() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("countrye").style.background = "red";
    var i6=document.getElementById("countrye").value;
    document.getElementById("countryediv").innerHTML ="";
    if(i6 == '')
    {
    var string6="You Need To Enter Your Country";
    document.getElementById("countryediv").innerHTML = string6 ;

            alert("Please Enter Your Country!");   
   
    }

    
}

function focusFunction7() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("citye").style.background = "yellow";
        }

function blurFunction7() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("citye").style.background = "red";
    var i7=document.getElementById("citye").value;
    document.getElementById("cityediv").innerHTML = "";
    if(i7 == '')
    {
    var string7="You Need To Enter Your City";
    document.getElementById("cityediv").innerHTML = string7 ;

            alert("Please Enter Your City!");   
   
    }

    
}

function focusFunction8() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("streete").style.background = "yellow";
        }

function blurFunction8() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("streete").style.background = "red";
    var i8=document.getElementById("streete").value;
    document.getElementById("streetediv").innerHTML ="";
    if(i8 == '')
    {
    var string8="You Need To Enter Your Street";
    document.getElementById("streetediv").innerHTML = string8 ;

            alert("Please Enter Your Street!");   
   
    }

    
}

function focusFunction9() 
        {
    // Focus = Changes the background color of input to yellow
    document.getElementById("balancee").style.background = "yellow";
        }

function blurFunction9() 
{
    // No focus = Changes the background color of input to red
    document.getElementById("balancee").style.background = "red";
    var i9=document.getElementById("balancee").value;
    document.getElementById("balanceediv").innerHTML = "";
    if(i9 == '')
    {
    var string9="You Need To Enter Your Balance";
    document.getElementById("balanceediv").innerHTML = string9 ;

            alert("Please Enter Your Balance!");   
   
    }

    
}

    </script>

    
<jsp:useBean id="userinfo" class="iti36.ecommerce.entity.User"></jsp:useBean> 
        
<jsp:setProperty property="*" name="userinfo"/> 

        
        You have entered below details:<br> 
<jsp:getProperty property="firstName" name="userinfo"/><br> 
<jsp:getProperty property="lastName" name="userinfo"/><br> 
<jsp:getProperty property="email" name="userinfo" /><br>
<jsp:getProperty property="password" name="userinfo" /><br>
<jsp:getProperty property="job" name="userinfo" /><br>
<jsp:getProperty property="street" name="userinfo" /><br>
<jsp:getProperty property="city" name="userinfo" /><br>
<jsp:getProperty property="country" name="userinfo" /><br>
<jsp:getProperty property="balance" name="userinfo" /><br>



</body>
</html>