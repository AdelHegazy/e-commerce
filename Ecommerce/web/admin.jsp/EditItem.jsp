<%-- 
    Document   : EditItem
    Created on : Mar 15, 2016, 8:32:17 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    
    <body>
        <!--form action="/Ecommerce/jsp/AdminItemGetData.jsp"method="post"--> 
        <form action="EditItemController" method="post"  enctype="MULTIPART/FORM-DATA"> 
        <table border="1" frame="box" style="width:100%" cellpadding="5" align="center">
	 <tbody>
	   <tr>
	     <td>name</td>
             <td>${requestScope.editItem.itemName}</td>
             <td><input type="text" size="30" name="ItemName"></td>
	   </tr>
           <tr>
	     <td>price</th>
             <td>${requestScope.editItem.itemPrice}</td>
             <td><input type="text" size="30" name="itemPrice"></td>
	   </tr>
           <tr>
	     <td>item Quantity</th>
             <td>${requestScope.editItem.itemQuantity}</td>
             <td><input type="text" size="30" name="itemQuantity"></td>
	   </tr>
           <tr>
	     <td>Item Desc</th>
             <td width="65%">${requestScope.editItem.itemDesc}</td>
             <td><input type="text" style="width:100%" name="itemDesc"></td>
	   </tr>
           <tr>  
	     <td>Item brand</th>
             <td>${requestScope.brand.brandName}</td>
             <td><select name="BrandId" style="width:100%">
                 <c:forEach var="brand" items="${applicationScope['brand']}">
                     <option  value="${brand.brandName}" >
                                        ${brand.brandName}
                     </option>
                 </c:forEach>
                 </select>
             </td>
	   </tr>
            <tr>
	     <td>item image</td>
             <td><img src="${requestScope.editItem.imageBase64Binary}" alt="" /></td>
             <td><input type="file" name="imageBase64Binary"/></td>
	   </tr>
           <c:if test="${requestScope.editItem.itemType=='Mobile'}">
          <tr>
	     <td>nETWORK Technology</td>
             <td>${requestScope.Sp.networkTechnology}</td>
             <td><input type="text" style="width:100%"name="networkTechnology"/></td>
	   </tr>
           <tr>
	     <td>launch Announced</td>
             <td>${requestScope.Sp.launchAnnounced}</td>
             <td><input type="text" style="width:100%"name="launchAnnounced"/></td>
	   </tr>
           <tr>
	     <td>body Weight </td>
             <td>${requestScope.Sp.bodyWeight}</td>
             <td><input type="text" style="width:100%"name="bodyWeight"/></td>
	   </tr>
           <tr>
	     <td>body Dimensions </td>
             <td>${requestScope.Sp.bodyDimensions}</td>
             <td><input type="text" style="width:100%"name="bodyDimensions"/></td>
	   </tr>
           <tr>
	     <td>body Build </td>
             <td>${requestScope.Sp.bodyBuild}</td>
             <td><input type="text" style="width:100%"name="bodyBuild"/></td>
	   </tr>
           <tr>
	     <td>body Sim </td>
             <td>${requestScope.Sp.bodySim}</td>
             <td><input type="text" style="width:100%"name="bodySim"/></td>
	   </tr>
            <tr>
	     <td>display Type </td>
             <td>${requestScope.Sp.displayType}</td>
             <td><input type="text" style="width:100%"/></td>
	   </tr>
           <tr>
	     <td>display Size </td>
             <td>${requestScope.Sp.displaySize}</td>
             <td><input type="text" style="width:100%"name="displaySize"/></td>
	   </tr>
           <tr>
	     <td>display Resolution </td>
             <td>${requestScope.Sp.displayResolution}</td>
             <td><input type="text" style="width:100%"name="displayResolution"/></td>
	   </tr>
           <tr>
	     <td>display Multitouch </td>
             <td>${requestScope.Sp.displayMultitouch}</td>
             <td><input type="text" style="width:100%"name="displayMultitouch"/></td>
	   </tr>
           <tr>
	     <td>display Protection </td>
             <td>${requestScope.Sp.displayProtection}</td>
             <td><input type="text" style="width:100%"name="displayProtection"/></td>
	   </tr>
           <tr>
	     <td>plat formOs </td>
             <td>${requestScope.Sp.platformOs}</td>
             <td><input type="text" style="width:100%"name="platformOs"/></td>
	   </tr>
           <tr>
	     <td>plat form Chipset </td>
             <td>${requestScope.Sp.platformChipset}</td>
             <td><input type="text" style="width:100%"name="platformChipset"/></td>
	   </tr>
           <tr>
	     <td>plat form Cpu</td>
             <td>${requestScope.Sp.platformCpu}</td>
             <td><input type="text" style="width:100%"name="platformCpu"/></td>
	   </tr>
           <tr>
	     <td>plat form Gpu</td>
             <td>${requestScope.Sp.platformGpu}</td>
             <td><input type="text" style="width:100%"name="platformGpu"/></td>
	   </tr>
           <tr>
	     <td>memory Cardslot</td>
             <td>${requestScope.Sp.memoryCardslot}</td>
             <td><input type="text" style="width:100%"name="memoryCardslot"/></td>
	   </tr>
           <tr>
	     <td>memory Internal</td>
             <td>${requestScope.Sp.memoryInternal}</td>
             <td><input type="text" style="width:100%"name="memoryInternal"/></td>
	   </tr>
           <tr>
	     <td>camera Primary</td>
             <td>${requestScope.Sp.cameraPrimary}</td>
             <td><input type="text" style="width:100%"name="cameraPrimary"/></td>
	   </tr>
            <tr>
	     <td>camera Features</td>
             <td>${requestScope.Sp.cameraFeatures}</td>
             <td><input type="text" style="width:100%"name="cameraFeatures"/></td>
	   </tr>
           <tr>
	     <td>camera Video</td>
             <td>${requestScope.Sp.cameraVideo}</td>
             <td><input type="text" style="width:100%"name="cameraVideo"/></td>
	   </tr>
           <tr>
	     <td>camera Secondary</td>
             <td>${requestScope.Sp.cameraSecondary}</td>
             <td><input type="text" style="width:100%"name="cameraSecondary"/></td>
	   </tr>
           <tr>
	     <td>sound Alert types</td>
             <td>${requestScope.Sp.soundAlerttypes}</td>
             <td><input type="text" style="width:100%"name="soundAlerttypes"/></td>
	   </tr>
           <tr>
	     <td>sound Loud speaker</td>
             <td>${requestScope.Sp.soundLoudspeaker}</td>
             <td><input type="text" style="width:100%" name="soundLoudspeaker"/></td>
	   </tr>
           <tr>
	     <td>mmJack</td>
             <td>${requestScope.Sp.mmJack}</td>
             <td><input type="text" style="width:100%"name="mmJack"/></td>
	   </tr>
           <tr>
	     <td>comms Wlan</td>
             <td>${requestScope.Sp.commsWlan}</td>
             <td><input type="text" style="width:100%"name="commsWlan"/></td>
	   </tr>
           <tr>
	     <td>comms Bluetooth</td>
             <td>${requestScope.Sp.commsBluetooth}</td>
             <td><input type="text" style="width:100%"name="commsBluetooth"/></td>
	   </tr>
           <tr>
	     <td>comms Gps</td>
             <td>${requestScope.Sp.commsGps}</td>
             <td><input type="text" style="width:100%"name="commsGps"/></td>
	   </tr>
           <tr>
	     <td>comms Nfc</td>
             <td>${requestScope.Sp.commsNfc}</td>
             <td><input type="text" style="width:100%"name="commsNfc"/></td>
	   </tr>
           <tr>
	     <td>comms Radio</td>
             <td>${requestScope.Sp.commsRadio}</td>
             <td><input type="text" style="width:100%" name="commsRadio"/></td>
	   </tr>
           </c:if>
            <tr>
               <td><input type="reset" style="width:100%"></td>
             <td><input type="submit"  style="width:100%"/></td>
	   </tr>
	  </tbody>
	</table>
             <input type="hidden" name="itemType" value="${requestScope.editItem.itemType}"/>
             <input type="hidden" name="ItemID" value=${requestScope.editItem.itemId}>
      </form>      
    </body>
</html>
