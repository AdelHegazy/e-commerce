<%-- 
    Document   : home
    Created on : Mar 6, 2016, 10:24:27 PM
    Author     : adel
--%>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Admin</title>
    </head>

    <body>
        <c:if test="${sessionScope.from eq 'cart'}">
            <script> chekCartState();</script>
            <c:remove var="from" scope="session"/>
        </c:if>
        <c:remove var="from" scope="session"/>

        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><i class="fa"></i>
                                        <c:if test="${sessionScope.loginUser != null}">
                                            Welcome ${sessionScope.loginUser.firstName}  ${sessionScope.loginUser.lastName}
                                        </c:if>
                                    </li>
                                </ul>
                                <ul class="nav nav-pills">
                                    <li>
                                        <c:if test="${sessionScope.loginUser != null}">
                                            your Balance is : ${sessionScope.loginUser.balance}
                                        </c:if>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="index.html"><img src="/Ecommerce/images/home/logo.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="admin/UserAdminController" class="active">Show Users</a></li>
                                    <li class="dropdown"><a href="admin/AddItemController">Add Item</a>
                                    </li> 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

    <center>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Items</h2>

                            <c:forEach items="${requestScope.items}" var="item">    
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">

                                                <img src="${item.imageBase64Binary}" alt="" />

                                                <h2>$<c:out value="${item.getItemPrice()}"/></h2>
                                                <p>${item.getItemName()}</p>
                                                <a href="admin/EditItem?itemId=${item.itemId}&type=${item.itemType}"class="btn btn-default add-to-cart">Edit</a> 
                                                <a href="admin/DeleteItem?itemId=${item.itemId}&type=${item.itemType}"class="btn btn-default add-to-cart">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </c:forEach>
                        </div><!--features_items-->

                        <ul class="pagination">
                            <c:forEach begin="0" end="${requestScope.pages - 1}" varStatus="loop">
                                <c:choose>
                                    <c:when test="${loop.count == 1}">
                                        <li id='${loop.count}'><a href="admin?page=${loop.count}">1</a></li>
                                        </c:when>    
                                        <c:otherwise>
                                        <li id='${loop.count}'><a href="admin?page=${loop.count}">${loop.count}</a></li>
                                        </c:otherwise>  
                                    </c:choose>
                                </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </center>
    <c:choose>
        <c:when test="${requestScope.pageNumber == 0}">
            <script>
                var li = document.getElementById("1");
                li.setAttribute("class", "active");
            </script>
        </c:when>    
        <c:otherwise>
            <c:set scope="page" var="listElement" value="${(requestScope.pageNumber / 12) + 1}" />
            <fmt:parseNumber var="listElementFmt" integerOnly="true" type="number" value="${pageScope.listElement}" />
            <script>
                    var list = document.getElementById("${pageScope.listElementFmt}");
                    list.setAttribute("class", "active");
            </script>
        </c:otherwise>  
    </c:choose>             
    <%@include file="../jsp/Footer.jsp" %>  
    <%@include file="../jsp/HomeHelper.jsp" %>
</body>
</html>
