<%-- 
    Document   : UserCartShop
    Created on : Mar 19, 2016, 11:37:41 AM
    Author     : yoka
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cart shop of user :${requestScope.userName}</h1>
        <table>
            <tr>
                <th>item name </th>
                <th>quantity</th>
                <th>
                    price
                </th>
            </tr>
            <c:forEach items="${requestScope.cartshop}" var="cart">
                  <tr>
                    <td>
                        ${cart.item.itemName}
                    </td>
                    <td>
                        ${cart.quantity}
                    </td>
                     <td>
                        ${cart.price}
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
