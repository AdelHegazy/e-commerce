<%-- 
    Document   : UsersPage
    Created on : Mar 18, 2016, 6:16:21 PM
    Author     : yoka
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Users </h1>
        <table border="1">
             <tr>
                 
                    <th>id</th>
               
                    <th>first Name</th>
                
                    <th>last Name</th>
              
                    <th>email</th>
               
                    <th>password</th>
               
                    <th>job</th>
               
                    <th>country</th>
                     <th>city</th>
                      <th>street</th>
                      <th>balance</th>
                    
                </tr>
            <c:forEach var="u" items="${requestScope.u}"  >
                <tr>
                 
                    <td>${u.id}</td>
               
                    <td>${u.firstName}</td>
                
                    <td>${u.lastName}</td>
              
                    <td>${u.email}</td>
               
                    <td>${u.password}</td>
               
                    <td>${u.job}</td>
               
                    <td>${u.country}</td>
                     <td>${u.city}</td>
                      <td>${u.street}</td>
                       <td>${u.balance}</td>
                       <td>
                         
                           <a href="UserAdmin2?type=1&uId=${u.id}">show history </a></td>
                        <td><a href="UserAdmin2?type=2&uId=${u.id}">show shopping Cart </a></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
