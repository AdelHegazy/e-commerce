<%-- 
    Document   : AddItem
    Created on : Mar 17, 2016, 6:34:28 PM
    Author     : Shika
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Item Admin</title>

        <script>
            function showSpecification() {
                var itemIndex = document.getElementById('checkType').selectedIndex;
                var itemValue = document.getElementById('checkType').options;
                if (itemValue[itemIndex].value == "Mobile") {
                    document.getElementById("specificationTable").style.visibility = "visible";
                    document.getElementById("accessoryType").style.visibility = "hidden";
                    document.getElementById("accessoryButton").style.visibility = "hidden";
                    document.getElementById("specificationButton").style.visibility = "visible";
                } else
                {
                    document.getElementById("specificationTable").style.visibility = "hidden";
                    document.getElementById("accessoryType").style.visibility = "visible";
                    document.getElementById("specificationButton").style.visibility = "hidden";
                    document.getElementById("accessoryButton").style.visibility = "visible";
                }
            }
        </script>
    </head>
    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><i class="fa"></i>
                                        <c:if test="${sessionScope.loginUser != null}">
                                            Welcome ${sessionScope.loginUser.firstName}  ${sessionScope.loginUser.lastName}
                                        </c:if>
                                    </li>
                                </ul>
                                <ul class="nav nav-pills">
                                    <li>
                                        <c:if test="${sessionScope.loginUser != null}">
                                            your Balance is : ${sessionScope.loginUser.balance}
                                        </c:if>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="index.html"><img src="/Ecommerce/images/home/logo.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="admin/UserAdminController" class="active">Show Users</a></li>
                                    <li class="dropdown"><a href="admin/AddItemController">Add Item</a>
                                    </li> 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

    <center>
        <div class="tab-content">
            <form action="AddItemController" method="post" enctype="MULTIPART/FORM-DATA">
                <table class="table_spec">
                    <tr class="th_title">
                        <th colspan="2">Item</th>
                    </tr>
                    <tr class="tr_el">
                        <td>Item Name</td>
                        <td><input type="text" name="itemName" value="" placeholder="Item Name" required="required"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Item Quantity</td>
                        <td><input type="number" name="itemQuantity" min="1" max="10000" step="1" value="1"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Item Price</td>
                        <td><input type="number" name="itemPrice" min="0" max="6000" step="1" value="1"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Item Type</td>
                        <td>
                            <select name="itemType" id= "checkType"  onchange="showSpecification()">
                                <option value="Mobile">Mobile</option>
                                <option value="accessories" selected="selected">Accessories</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="tr_el">
                        <td>brand</td>
                        <td>
                            <select name="brandName" id="myBrand">
                                <c:forEach var="brand" items="${applicationScope['brand']}">
                                    <option value="${brand.brandName}">${brand.brandName}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr class="tr_el">
                        <td>Item Image</td>
                        <td><input type="file" name="image" accept="image/*" required="required"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Item Descrition</td>
                        <td><textarea rows="4" cols="50" name="descrition" required="required"></textarea></td>
                    </tr>
                    <tr class="tr_el" id="accessoryType" style="visibility: visible">
                        <td>Accessories type</td>
                        <td><input type="text" name="accessoryType" value="" placeholder="Accessories type"/></td>
                    </tr>
                    <tr id="accessoryButton" style="visibility: visible">
                        <td colspan="2"><center><br><input type="submit" value="Add Item"/><br></center></td>
                    </tr>
                </table>

                <table class="table_spec" id="specificationTable" style="visibility:hidden">
                    <tr class="th_title">
                        <th colspan="2">
                            Basic Specifications</th>
                    </tr>
                    <tr class="tr_el">
                        <td>NETWORK Technology</td>
                        <td><input type="text" name="networkTechnology" value="" placeholder="Network Technology"/></td>
                    </tr>
                    <tr>
                        <td>Announced</td>
                        <td><input type="text" name="announced" value="" placeholder="Announced"/></td>
                    </tr>	
                    <tr>
                        <td>Operating System</td>
                        <td><input type="text" name="os" value="" placeholder="Operating System"/></td>
                    </tr>	
                    </tr>

                    <tr class="th_title">
                        <th colspan="2">Display</th>
                    </tr>
                    <tr class="tr_el">
                        <td>Screen Size</td>
                        <td><input type="text" name="screenSize" value="" placeholder="Screen Size"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Screen Type</td>
                        <td><input type="text" name="screenType" value="" placeholder="Screen Type"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Sensors</td>
                        <td><input type="text" name="Sensors" value="" placeholder="Sensors"/></td>
                    </tr>
                    <tr class="th_title">
                        <th colspan="2">Sound</th>
                    </tr>
                    <tr class="tr_el">
                        <td>Alert types</td>
                        <td><input type="text" name="alert" value="" placeholder="Alert Type"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Loudspeaker</td>
                        <td><input type="text" name="loudspeaker" value="" placeholder="Loudspeaker"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>3.5mm jack</td>
                        <td><input type="text" name="jack" value="" placeholder="jack"/></td>
                    </tr>
                    <tr class="th_title">
                        <th colspan="2">Memory</th>
                    </tr>
                    <tr class="tr_el">
                        <td>Card slot</td>
                        <td><input type="text" name="CardSlot" value="" placeholder="Card slot"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Internal</td>
                        <td><input type="text" name="internal" value="" placeholder="Internal Memory"/></td>
                    </tr>
                    <tr class="th_title">
                        <th colspan="2">Data</th>
                    </tr>
                    <tr class="tr_el">
                        <td>GPS</td>
                        <td><input type="text" name="GPS" value="" placeholder="GPS"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Wi-Fi</td>
                        <td><input type="text" name="wifi" value="" placeholder="Wi-Fi"/></td>
                    </tr>
                    <tr class="tr_el">
                        <td>Bluetooth</td>
                        <td><input type="text" name="bluetooth" value="" placeholder="Bluetooth"/></td>
                    </tr>
                    <tr class="th_title">
                        <th colspan="2">Camera</th>
                    </tr>
                    <tr class="tr_el">
                        <td>Primary</td>
                        <td><input type="text" name="primary" value="" placeholder="Primary"/></td>
                    </tr>

                    <tr class="tr_el">
                        <td>Secondary</td>
                        <td><input type="text" name="secondary" value="" placeholder="Secondary"/></td>
                    </tr>
                    <tr class="th_title">

                        <th colspan="2"> Processor</th>
                    </tr>
                    <tr class="tr_el">
                        <td>CPU</td>
                        <td><input type="text" name="CPU" value="" placeholder="CPU"/></td>
                    </tr>

                    <tr class="tr_el">
                        <td>GPU</td>
                        <td><input type="text" name="GPU" value="" placeholder="GPU"/></td>
                    </tr>         
                    <tr id="specificationButton" style="visibility:hidden">
                        <td colspan="2"><center><br><input type="submit" value="Add Item"/><br></center></td>
                    </tr>
                </table>                              
            </form>
        </div>                           
    </center>

    <%@include file="../jsp/Footer.jsp" %> 
    <%@include file="../jsp/HomeHelper.jsp" %>
</body>
</html>
