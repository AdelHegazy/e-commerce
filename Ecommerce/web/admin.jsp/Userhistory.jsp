<%-- 
    Document   : Userhistory
    Created on : Mar 19, 2016, 11:37:28 AM
    Author     : yoka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>History of user :${requestScope.userName}</h1>
        <table>
            <tr>
                <th>item Name</th>
                <th>price</th>
                <th>quantity</th>
                <th>Date </th>


            </tr>
            <c:forEach var="cart" items="${requestScope.history}" >
                <tr>
                    <td>
                        ${cart.item.itemName}
                    </td> 
                    <td>
                        ${cart.price}
                    </td>
                    <td>
                        ${cart.quantity}
                    </td>
                    <td>
                        ${cart.date}
                    </td>
                </tr>
            </c:forEach>
        </table>

    </body>
</html>
