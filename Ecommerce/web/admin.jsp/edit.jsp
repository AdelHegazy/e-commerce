<%-- 

    Document   : EditItemTest2
    Created on : Mar 19, 2016, 10:40:26 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Item | E-Shopper</title>
        <link href="/Ecommerce/css/main.css" rel="stylesheet">
        <link href="/Ecommerce/css/responsive.css" rel="stylesheet">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        
          
        <script>
            function showSpecification() {
                var itemIndex = document.getElementById('checkType').selectedIndex;
                var itemValue = document.getElementById('checkType').options;
                if (itemValue[itemIndex].value == "Mobile") {
                    document.getElementById("specificationTable").style.visibility = "visible";
                    document.getElementById("accessoryType").style.visibility = "hidden";
                } else
                {
                    document.getElementById("specificationTable").style.visibility = "hidden";
                    document.getElementById("accessoryType").style.visibility = "visible";
                }
            }
        </script>
    </head>
    <body>
       
        <form action="EditItemController" method="post"  enctype="MULTIPART/FORM-DATA">
        <div class="tab-content">
            
                <div class="tab-pane fade" id="details" >
                    <table class="table_spec">
                        <tr class="th_title">
                            <th colspan="2">Item</th>
                        </tr>
                        <tr class="tr_el">
                            <td>Item Name</td>
                            <td><input type="text" name="ItemName" required="required" value="${requestScope.editItem.itemName}"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>Item Quantity</td>
                            <td><input type="number" name="itemQuantity" min="0" max="10000" step="1" value="${requestScope.editItem.itemQuantity}"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>Item Price</td>
                            <td><input type="number" name="itemPrice" min="0" max="6000" step="1" value="${requestScope.editItem.itemPrice}"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>Item Type</td>
                            <td>${requestScope.editItem.itemType}</td>
                                
                        </tr>
                        <tr class="tr_el">
                            <td>brand</td>
                            <td>
                                <select name="BrandId" id="myBrand">
                                    <c:forEach var="brand" items="${applicationScope['brand']}">
                                        <option value="${brand.brandName}">${brand.brandName}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr class="tr_el">
                            <td>Item Image</td>
                            <td><img src="${requestScope.editItem.imageBase64Binary}" /></td>
                            <td><input type="file" name="imageBase64Binary" accept="image/*" required="required"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>Item Descrition</td>
                            <td><textarea rows="4" cols="50" name="itemDesc" required="required" >${requestScope.editItem.itemDesc}</textarea></td>
                        </tr>
                       
                        
                        <tr>
                            <c:if test="${requestScope.editItem.itemType=='accessorie'}">
                            <td>Accessories type</td>
                            <td><input type="text" name="accessoryType" value="" placeholder="Accessories type"/></td>
                        </c:if> 
                        </tr>
                        
                    </table>
                </div>
                       <c:if test="${requestScope.editItem.itemType=='Mobile'}">       
                <div class="tab-pane fade" id="details" >
                    <table class="table_spec" id="specificationTable" >
                        <tr class="th_title">
                            <th colspan="2">
                                Basic Specifications</th>
                        </tr>
                        <tr class="tr_el">
                            <td>NETWORK Technology</td>
                            <td><input type="text" name="networkTechnology" value="${requestScope.Sp.networkTechnology}"/></td>
                        </tr>
                        <tr>
                            <td>Announced</td>
                            <td><input type="text" name="launchAnnounced" value="${requestScope.Sp.launchAnnounced}"/></td>
                        </tr>	
                        	
                        </tr>

                        <tr class="th_title">
                            <th colspan="2">Display</th>
                        </tr>
                        <tr class="tr_el">
                            <td>display Resolution</td>
                            <td><input type="text" name="displayResolution" value="${requestScope.Sp.displayResolution}"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>Screen Type</td>
                            <td><input type="text" name="displayType" value="${requestScope.Sp.displayType}"/></td>
                        </tr>
                        <tr class="th_title">
                            <th colspan="2">Sound</th>
                        </tr>
                       
                        
                        <tr class="th_title">
                            <th colspan="2">Memory</th>
                        </tr>
                        
                        <tr class="tr_el">
                            <td>Internal</td>
                            <td><input type="text" name="memoryInternal" value="${requestScope.Sp.memoryInternal}"/></td>
                        </tr>
                        <tr class="th_title">
                            <th colspan="2">body</th>
                        </tr>
                        <tr class="tr_el">
                            <td>bodyWeight</td>
                            <td><input type="text" name="bodyWeight" value="${requestScope.Sp.bodyWeight}"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>bodyBuild</td>
                            <td><input type="text" name="bodyBuild" value="${requestScope.Sp.bodyBuild}"/></td>
                        </tr>
                        <tr class="tr_el">
                            <td>body Sim</td>
                            <td><input type="text" name="bodySim" value="${requestScope.Sp.bodySim}"/></td>
                        </tr> 
                        <tr class="tr_el">
                            <td>body Dimensions</td>
                            <td><input type="text" name="bodyDimensions" value="${requestScope.Sp.bodyDimensions}"/></td>
                        </tr>
                        <tr class="th_title">
                            <th colspan="2">Camera</th>
                        </tr>
                        <tr class="tr_el">
                            <td>Primary</td>
                            <td><input type="text" name="cameraPrimary" value="${requestScope.Sp.cameraPrimary}"/></td>
                        </tr>

                        <tr class="tr_el">
                            <td>Secondary</td>
                            <td><input type="text" name="cameraSecondary" value="${requestScope.Sp.cameraSecondary}"/></td>
                        </tr>
                        <tr class="th_title">

                            <th colspan="2"> Processor</th>
                        </tr>
                        <tr class="tr_el">
                            <td>CPU</td>
                            <td><input type="text" name="platformCpu" value="${requestScope.Sp.platformCpu}"/></td>
                        </tr>
                         </c:if> 
                        
                        <tr class="th_title">

                            <th colspan="2"><input type="submit" value="Edit Item" /></th>
                        </tr>
                       
                    </table>
                </div>
               
                   </div>     
                         
                        
                 <input type="hidden" name="ItemID" value=${requestScope.editItem.itemId}>
                 <input type="hidden" name="itemType" value="${requestScope.editItem.itemType}"/>
          </form> 
    </body>
</html>
