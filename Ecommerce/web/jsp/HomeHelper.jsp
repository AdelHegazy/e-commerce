<%-- 
    Document   : homeimport
    Created on : Mar 7, 2016, 1:11:44 AM
    Author     : adel
--%>

<link href="/Ecommerce/css/bootstrap.min.css" rel="stylesheet">
<link href="/Ecommerce/css/font-awesome.min.css" rel="stylesheet">
<link href="/Ecommerce/css/prettyPhoto.css" rel="stylesheet">
<link href="/Ecommerce/css/price-range.css" rel="stylesheet">
<link href="/Ecommerce/css/animate.css" rel="stylesheet">
<link href="/Ecommerce/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-default.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-bottom-right-corner.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-flat-attack.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-os.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-plain.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-top.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex-theme-wireframe.css" />
<link rel="stylesheet" href="/Ecommerce/css/vex.css" />

<link href="/Ecommerce/css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->       
<link rel="shortcut icon" href="/Ecommerce/images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/Ecommerce/images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/Ecommerce/images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/Ecommerce/images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="/Ecommerce/images/ico/apple-touch-icon-57-precomposed.png">
<script src="/Ecommerce/js/jquery.js"></script>
<script src="/Ecommerce/js/bootstrap.min.js"></script>
<script src="/Ecommerce/js/jquery.scrollUp.min.js"></script>
<script src="/Ecommerce/js/price-range.js"></script>
<script src="/Ecommerce/js/jquery.prettyPhoto.js"></script>
<script src="/Ecommerce/js/main.js"></script>
<script src="/Ecommerce/js/tinybox.js"></script>
<script src="/Ecommerce/js/bootbox.min.js"></script>
<script src="/Ecommerce/js/popup.js"></script>
<script src="/Ecommerce/js/vex.js"></script>
<script src="/Ecommerce/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-os';</script>

<script src="/Ecommerce/js/countries.js"></script>