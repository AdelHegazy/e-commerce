<!DOCTYPE html>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Cart | E-Shopper</title>
   <%@include file="HomeHelper.jsp" %>
    </head><!--/head-->
    <body>
        <%@include file="Header.jsp" %>
        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Shopping Cart</li>
                    </ol>
                </div>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Item</td>
                                <td class="description"></td>
                                <td class="price">Price</td>
                                <td class="quantity">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:set var="totalOfItems" value="${0}"/>
                            <c:if test="${requestScope.cnt != 0}"> 
                                <c:forEach var="i" begin="0" end="${requestScope.cnt-1}">
                                    <tr>
                                        <td class="cart_product">
                                            <a href=""><img src="${requestScope.cartObjcts.get(i).getItem().getImageBase64Binary()}" alt=""></a>
                                        </td>
                                        <td class="cart_description">
                                            <h4><a href="">${requestScope.cartObjcts.get(i).getItem().getItemName()}</a></h4>
                                        </td>
                                        <td class="cart_price">
                                            <p>$${requestScope.cartObjcts.get(i).getPrice()}</p>
                                        </td>
                                        <td class="cart_quantity">
                                            <form>
                                                <div class="cart_quantity_button">
                                                    <a class="cart_quantity_up" href="CartShopController?action=add&itemId=${requestScope.cartObjcts.get(i).getItem().itemId}&indx=${i}" > + </a>
                                                    <input class="cart_quantity_input" type="text" name="quantity" value="${requestScope.cartObjcts.get(i).quantity}" autocomplete="off" size="2">
                                                    <a class="cart_quantity_down" href="CartShopController?action=sub&itemId=${requestScope.cartObjcts.get(i).getItem().itemId}&indx=${i}"> - </a>

                                                </div>

                                        </td>
                                        <td class="cart_total">

                                            <c:set var="totalBeforeTax" value="${requestScope.cartObjcts.get(i).quantity*requestScope.cartObjcts.get(i).getPrice()}"></c:set>
                                            <p class="cart_total_price">$<c:out value="${totalBeforeTax}"/></p>
                                            <c:set var="totalOfItems" value="${totalBeforeTax+totalOfItems}"/>
                                        </td>
                                        <td class="cart_delete">
                                            <a class="cart_quantity_delete" href="CartShopController?action=del&itemId=${requestScope.cartObjcts.get(i).getItem().itemId}&indx=${i}"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>      



                        </tbody>
                    </table>
                </div>
            </div>
        </section> <!--/#cart_items-->

        <section id="do_action">
            <div class="container">

                <div class="row">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <c:if test="${requestScope.cnt != 0}">
                            <div class="total_area" align='mid'>
                                <ul>
                                    <li>Cart Sub Total <span>$<c:out value="${totalOfItems}"/></span></li>
                                    <li>Eco Tax <span>$2</span></li>
                                    <li>Shipping Cost <span>Free</span></li>
                                    <li>Total <span id="total">${totalOfItems+2}</span></li>
                                </ul>

                                <a class="btn btn-default check_out" href="CheckOutController?id=${requestScope.cartObjcts.get(0).users.id}&total=${totalOfItems+2}">Check Out</a>
                            </div>
                        </c:if>      
                    </div>
                </div>
            </div>
        </section><!--/#do_action-->

        <%@include file="Footer.jsp" %>
        <%@include file="HomeHelper.jsp" %>

    </body>
</html>