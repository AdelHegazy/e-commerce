<%-- 
    Document   : header
    Created on : Mar 7, 2016, 1:01:34 AM
    Author     : adel
--%>

<!DOCTYPE html>
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><i class="fa"></i>
                            <c:if test="${sessionScope.loginUser != null}">
                                Welcome ${sessionScope.loginUser.firstName}  ${sessionScope.loginUser.lastName}
                            </c:if>
                            </li>
                        </ul>
                        <ul class="nav nav-pills">
                            <li>
                            <c:if test="${sessionScope.loginUser != null}">
                                your Balance is : ${sessionScope.loginUser.balance}
                            </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="#"><img src="/Ecommerce/images/home/logo.png" alt="" /></a>
                    </div>

                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <c:if test="${sessionScope.loginUser == null}">
                            <li><a href="NewUser"><i class="fa fa-user"></i>New User</a></li>  
                            </c:if>

                            <li><a href="CartShopController"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                            <c:if test="${sessionScope.loginUser != null}">
                                <li><a href="CheckOutController?id=${sessionScope.loginUser.id}"><i class="fa fa-crosshairs"></i> History</a></li>  
                            </c:if>
                            <c:if test="${sessionScope.loginUser == null}">
                                <li><a href="" onclick="login();return false;"><i class="fa fa-lock"></i> Login</a></li>
                            </c:if>  

                            <c:if test="${sessionScope.loginUser != null}">
                                <li><a href="jsp/Account.jsp"><i class="fa fa-user"></i> Account</a></li>
                                <li><a href="LoginController?logout=yes"><i class="fa fa-lock"></i> Logout</a></li>
                            </c:if>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="HomeController" class="active">Home</a></li>
                            <li class="dropdown"><a href="ShopController">Shop</a>
                            </li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

