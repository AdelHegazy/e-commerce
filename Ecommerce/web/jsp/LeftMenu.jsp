<%-- 
    Document   : leftmenu
    Created on : Mar 7, 2016, 12:44:58 AM
    Author     : adel
--%>

<%@page contentType="text/html" %>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Category</h2>
        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            Mobiles
                        </a>
                    </h4>
                </div>
                <div id="sportswear" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <c:forEach var="brand" items="${applicationScope['brand']}">
                                <li id= "item"><a  href="<c:url value="/SearchController?type=mobile&&id=${brand.id}&&val=0:600" />">
                                        ${brand.brandName}
                                    </a>
                                </li>
                             </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordian" href="#mens">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            Accessories
                        </a>
                    </h4>
                </div>
                <div id="mens" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                               <c:forEach var="name" items="${applicationScope['accessorie']}">
                                <li id= "item"><a href="<c:url value="/SearchController?name=${name}&type=accessorie&&val=0" />">
                                        ${name}
                                    </a></li>
                                </c:forEach>

                        </ul>
                    </div>
                </div>
            </div>



        </div><!--/category-products-->



        <div class="price-range"><!--price-range-->
            <h2>Price Range</h2>
            <div class="well text-center">
                <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="5000" data-slider-step="100" data-slider-value="[0,20000]" id="sl2" ><br />
                <b class="pull-left">$ 0</b> <b class="pull-right">$ 5000</b>
            </div>
        </div><!--/price-range-->

        <div class="shipping text-center"><!--shipping-->
            <img src="/Ecommerce/images/home/shipping.jpg" alt="" />
        </div><!--/shipping-->

    </div>
</div>
