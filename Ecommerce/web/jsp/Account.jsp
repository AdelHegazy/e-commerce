<%-- 
    Document   : CheckOut
    Created on : Mar 14, 2016, 9:27:49 PM
    Author     : adel
--%>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Account</title>
        <script src="/Ecommerce/js/validation.js"></script>
    </head>
    <%@include file="Header.jsp" %>
    <body>

        <form name="AccountForm" class="containerss register "  method="post" action="/Ecommerce/AccountController" onsubmit="return validateAccount();"> 
            <table border="1" align="center" name="table">
                <tr>
                    <td colspan="2" align="center">Account</td>
                </tr>

                <tr>
                    <td>Card&nbsp;number</td>
                    <td>   <input type="text" size="25"  name="CardNumber" id="CardNumber"/> <label id="CardNumberNoValue"></label></td>
                </tr>

                <tr>
                    <td colspan="3" align="center"> <input type="submit" value="Submit"></td>
                </tr>
                <tr>

                </tr>
                <tr>
                    
                </tr>
                <tr>
                    <td id="CardValidationtError" colspan="3" align="center" style="color: red; position: relative; top: 9px;">
                        <c:if test="${CardValidationtError!= null}">
                            ${CardValidationtError}
                        </c:if>
                    </td>
                </tr>
            </table>
        </form>
        <%@include file="Footer.jsp" %>
        <%@include file="HomeHelper.jsp" %>
        <link rel="stylesheet" href="/Ecommerce/css/styleee.css">
    </body>

</html>