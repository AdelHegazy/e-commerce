<%-- 
    Document   : CheckOut
    Created on : Mar 14, 2016, 9:27:49 PM
    Author     : adel
--%>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CheckOut</title>
        <%@include file="HomeHelper.jsp" %>
    </head>

    <body>
        <%@include file="Header.jsp" %>
        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Shopping Cart</li>
                    </ol>
                </div>
                <c:choose>
                    <c:when test="${requestScope.itemError != null}">
                         Quantity of this product is ${requestScope.itemError.itemName} is not Enough.
                    </c:when>
                    <c:when test="${requestScope.history != null}">
                        <c:forEach var="row" items="${requestScope.history}">
                            <div class="table-responsive cart_info">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr class="cart_menu">
                                            <td class="image">Item</td>
                                            <td class="description"></td>
                                            <td class="price">Price</td>
                                            <td class="quantity">Quantity</td>
                                            <td class="total">Total</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="cart_product">
                                                <img src="${row.item.imageBase64Binary}" alt=""></a>
                                            </td>
                                            <td class="cart_description">
                                                <h4>${row.item.itemName}</h4>
                                                <p></p>
                                            </td>
                                            <td class="cart_price">
                                                <p>${row.item.itemPrice}</p>
                                            </td>
                                            <td class="cart_quantity">
                                                <div class="cart_quantity_button">
                                                    <p>${row.quantity}</p>
                                                </div>
                                            </td>
                                            <td class="cart_total">
                                                <p class="cart_total_price">${row.price}</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>     
                        </c:forEach>
                    </c:when>
                   <c:when test="${requestScope.checkBalance != null}">
                         "check your Balance please"
                    </c:when>
                </c:choose>
        </section> <!--/#cart_items-->
        <%@include file="Footer.jsp" %>
        <%@include file="HomeHelper.jsp" %>
    </body>
</html>