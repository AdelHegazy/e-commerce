<%-- 
    Document   : home
    Created on : Mar 6, 2016, 10:24:27 PM
    Author     : adel
--%>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home</title>
    <%@include file="HomeHelper.jsp" %>
</head>


<header id="home-header">
    <%@include file="Header.jsp" %>    
</header>
<section id="slider"><!--slider-->

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#slider-carousel" data-slide-to="1"></li>
                        <li data-target="#slider-carousel" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-6">
                                <h1><span>E</span>-SHOPPER</h1>
                                
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <button type="button" class="btn btn-default get">Get it now</button>
                            </div>
                            <div class="col-sm-6">
                                <img src="/Ecommerce/images2/grid-img1.jpg" class="girl img-responsive" alt="" />
                                <img src="/Ecommerce/images/home/pricing.png"  class="pricing" alt="" />
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-6">
                                <h1><span>E</span>-SHOPPER</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <button type="button" class="btn btn-default get">Get it now</button>
                            </div>
                            <div class="col-sm-6">
                                <img src="/Ecommerce/images2/grid-img2.jpg" class="girl img-responsive" alt="" />
                                <img src="/Ecommerce/images/home/pricing.png"  class="pricing" alt="" />
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-sm-6">
                                <h1><span>E</span>-SHOPPER</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <button type="button" class="btn btn-default get">Get it now</button>
                            </div>
                            <div class="col-sm-6">
                                <img src="/Ecommerce/images2/grid-img3.jpg" class="girl img-responsive" alt="" />
                                <img src="/Ecommerce/images/home/pricing.png" class="pricing" alt="" />
                            </div>
                        </div>

                    </div>

                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section><!--/slider-->
<section>
    <div class="container">
        <div class="row">
            <%@include file="LeftMenu.jsp" %>
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Items</h2>

                    <c:forEach var="row" items="${requestScope.item}">
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <a href="<c:url value="/ItemDescription?id=${row.itemId}" />" >
                                            <img src="${row.imageBase64Binary}" alt="" />
                                        </a>
                                        <h2>${row.itemPrice}</h2>
                                        <p>${row.itemName}</p>
                                        <a  href="CartShopController?action=addToCart&itemId=${row.itemId}&from=Home" class="btn btn-default add-to-cart" ><i class="fa fa-shopping-cart"></i>Add to cart</a>     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>

<c:if test="${sessionScope.from eq 'cart'}">

    <c:if test="${sessionScope.loginUser == null}">
        <script>mustLogin();</script>
        <c:remove var="from" scope="session"/>
    </c:if>   

    <c:if test="${sessionScope.loginUser != null}">
        <script>chekCartState();</script>
        <c:remove var="from" scope="session"/>
    </c:if>
</c:if>
        
<c:if test="${sessionScope.to eq 'Account'}">
    <script>congrats();</script>
    <c:remove var="to" scope="session"/>
</c:if>
    
<c:if test="${sessionScope.login eq 'invalidLogin'}">
    <script>invalidLogin();</script>
    <c:remove var="login" scope="session"/>
</c:if>

<%@include file="Footer.jsp"%>  
<%@include file="HomeHelper.jsp"%> 