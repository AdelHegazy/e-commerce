<%-- 

    Document   : Store
    Created on : Mar 4, 2016, 7:41:07 AM
    Author     : Shika
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shop | E-Shopper</title>
    </head>
    <body>
        <c:if test="${sessionScope.from eq 'cart'}">

            <c:if test="${sessionScope.loginUser == null}">
                <script>mustLogin();</script>
                <c:remove var="from" scope="session"/>
            </c:if>   

            <c:if test="${sessionScope.loginUser != null}">
                <script>chekCartState();</script>
                <c:remove var="from" scope="session"/>
            </c:if>
        </c:if>

        <%@include file="Header.jsp" %>

        <section>
            <div class="container">
                <div class="row">
                    <%@include file="LeftMenu.jsp" %>

                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Features Items</h2>

                            <c:forEach items="${requestScope.items}" var="item">    
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <a href="<c:url value="/ItemDescription?id=${item.getItemId()}" />" >
                                                    <img src="${item.imageBase64Binary}" alt="" />   
                                                </a>
                                                <h2>$<c:out value="${item.getItemPrice()}"/></h2>
                                                <p>${item.getItemName()}</p>
                                                <a href="CartShopController?action=addToCart&itemId=${item.getItemId()}&from=shop" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </c:forEach>
                        </div><!--features_items-->

                        <ul class="pagination">
                            <c:forEach begin="0" end="${requestScope.pages - 1}" varStatus="loop">
                                <c:choose>
                                    <c:when test="${loop.count == 1}">
                                        <li id='${loop.count}'><a href="ShopController?page=${loop.count}">1</a></li>
                                        </c:when>    
                                        <c:otherwise>
                                        <li id='${loop.count}'><a href="ShopController?page=${loop.count}">${loop.count}</a></li>
                                        </c:otherwise>  
                                    </c:choose>
                                </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <c:choose>
            <c:when test="${requestScope.pageNumber == 0}">
                <script>
                    var li = document.getElementById("1");
                    li.setAttribute("class", "active");
                </script>
            </c:when>    
            <c:otherwise>
                <c:set scope="page" var="listElement" value="${(requestScope.pageNumber / 12) + 1}" />
                <fmt:parseNumber var="listElementFmt" integerOnly="true" type="number" value="${pageScope.listElement}" />
                <script>
                    var list = document.getElementById("${pageScope.listElementFmt}");
                    list.setAttribute("class", "active");
                </script>
            </c:otherwise>  
        </c:choose>
        <%@include file="HomeHelper.jsp"%>
        <%@include file="Footer.jsp" %>

    </body>
</html>
