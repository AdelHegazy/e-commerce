<%-- 
    Document   : CheckOut
    Created on : Mar 14, 2016, 9:27:49 PM
    Author     : adel
--%>

<%@page contentType="text/html"  pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CheckOut</title>
        <script src="/Ecommerce/js/validation.js"></script>
    </head>
    <%@include file="Header.jsp" %>
    <body>

        <form name="myform" class="containerss register "  method="post" action="SignupController" onsubmit="return validateForm()"> 
            <table border="1" align="center" name="table">
                <tr>
                    <td colspan="2" align="center">Registration</td>
                </tr>

                <tr>
                    <td>First&nbsp;Name</td>
                    <td>   <input type="text" size="25"  placeholder="First Name" name="firstname" id="firstname"/> <label id="fname"></label></td>
                </tr>

                <tr>
                    <td>Last&nbsp;Name</td>

                    <td>  <input type="text" size="25"  placeholder="Last Name" name="lastname" id="lastname"> <label id="lname"></label> </td>
                </tr>


                <tr>
                    <td>Email</td>

                    <td>  <input type="text" size="25" placeholder="Email" name="email" id="email"> <label id="lemail"></label> </td>
                </tr>

                <tr>
                    <td>Password</td>

                    <td>  <input type="password" size="25" placeholder="Password" name="pass" id="pass"> <label id="pa"></label></td>
                </tr>
                <tr>
                    <td>Job</td>

                    <td>  <input type="text" size="25" placeholder="Job" name="m" id="m"> <label id="lm"></label></td>
                </tr>
                <tr>
                    <td>Country</td> 
                    <td> <select id="country" name ="country"></select><label id="cou"></label></td>
                </tr>
                <tr>
                    <td>City</td> 
                    <td><select name ="state" id ="state"></select><label id="lci"></label></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"> <input type="submit" value="Submit"></td>
                </tr>
                <tr>
                    
                </tr>
                <tr>
                    <td colspan="3" align="center" style="color: red">
                        <c:if test="${error!= null}">
                            ${error}
                        </c:if>
                    </td>
                </tr>
            </table>
        </form>
        <%@include file="Footer.jsp" %>
        <%@include file="HomeHelper.jsp" %>
        <link rel="stylesheet" href="/Ecommerce/css/styleee.css">
        <script language="javascript">
            populateCountries("country", "state");
        </script>

    </body>

</html>