<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Product Details | E-Shopper</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <%@include file="Header.jsp" %>
        <section>
            <div class="container">
                <div class="row">
                    <%@include file="LeftMenu.jsp" %>

                    <div class="col-sm-9 padding-right">
                        <div class="product-details"><!--product-details-->
                            <div class="col-sm-5">
                                <div class="view-product">
                                    <!--           image of the product                    -->

                                    <img src="${item.imageBase64Binary}" alt="" />

                                </div>
                                <div id="similar-product" class="carousel slide" data-ride="carousel">

                                    <!-- Wrapper for slides it can be colors
                                      <div class="carousel-inner">
                                                  <div class="item active">
                                                    <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
                                                  </div>
                                                  <div class="item ">
                                                    <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
                                                  </div>
                                                  <div class="item ">
                                                    <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
                                                  </div>
                                                  
                                          </div>
                                    -->
                                    <!-- Controls -->
                                    <a class="left item-control" href="#similar-product" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right item-control" href="#similar-product" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>

                            </div>
                            <div class="col-sm-7">
                                <div class="product-information"><!--/product-information-->


                                    <img src="images/product-details/new.jpg" class="newarrival" alt="" />


                                    <!--           Name of the product                    -->
                                    <h2>${item.itemName}</h2>

                                    <span>
                                        <span>price :${item.itemPrice}</span>
                                        <label>Quantity:</label>
                                        <label>${item.itemQuantity}</label>

                                        <a  href="CartShopController?action=addToCart&itemId=${item.itemId}&from=Home" class="btn btn-default add-to-cart" >
                                            <i class="fa fa-shopping-cart"></i>Add to cart</a>     


                                    </span>
                                    <c:if test="${item.itemQuantity==0}">
                                        <p><b>Availability:</b> Out Stock</p>
                                    </c:if>
                                    <c:if test="${item.itemQuantity>0}">
                                        <p><b>Availability:</b> In Stock</p>
                                    </c:if>
                                    <p><b>Brand:</b> ${item.brand.brandName}</p>

                                </div><!--/product-information-->
                            </div>
                        </div><!--/product-details-->
                        <c:if test="${item.itemType=='Mobile'}">	
                            <div class="category-tab shop-details-tab"><!--category-tab-->
                                <div class="col-sm-12">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#details" data-toggle="tab" >Details</a></li>
                                        <li><a href="#companyprofile" data-toggle="tab">Recommended Items</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <!--<div class="table-responsive cart_info" id="details" >-->
                                    <div class="tab-pane fade active in" id="details" >
                                        <table class="table_spec" >

                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Basic Specifications</th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>NETWORK Technology</td>
                                                <td>${spec.networkTechnology}</td>
                                            </tr>
                                            <tr>
                                                <td>Announced
                                                </td>
                                                <td>	${spec.launchAnnounced}	</td>	</tr>	
                                            <tr>
                                                <td>Operating System
                                                </td>
                                                <td>	${spec.platformOs}	</td></tr>	
                                            </tr>

                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Display</th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Screen Size</td>
                                                <td>${spec.displaySize}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Screen Type</td>
                                                <td>${spec.displayType}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Screen Resolution</td>
                                                <td>${spec.displayResolution}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td> Multi-touch</td>
                                                <td>${spec.displayMultitouch}</td>
                                            </tr>
<!--                                            <tr class="tr_el">
                                                <td> Protection</td>
                                                <td>${spec.displayProtection}</td>
                                            </tr>-->

                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Sound</th>
                                            </tr>
                                      
                                            <tr class="tr_el">
                                                <td>Loudspeaker</td>
                                                <td>${spec.soundLoudspeaker}</td>
                                            </tr>
                                           
                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Memory</th>
                                            </tr>
                                     
                                            <tr class="tr_el">
                                                <td>Internal</td>
                                                <td>	${spec.memoryInternal}	</td>
                                            </tr>

                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Data</th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>GPRS</td>
                                                <td>${spec.commsGps}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Wi-Fi</td>
                                                <td>${spec.commsWlan}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Bluetooth</td>
                                                <td>${spec.commsBluetooth}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Nfc</td>
                                                <td>${spec.commsNfc}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Radio</td>
                                                <td>${spec.commsRadio}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Usb</td>
                                                <td>${spec.commsUsb}</td>
                                            </tr>

                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Camera</th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Primary</td>
                                                <td>${spec.cameraPrimary}</td>
                                            </tr>

                                            <tr class="tr_el">
                                                <td>Secondary</td>
                                                <td>${spec.cameraSecondary}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Features</td>
                                                <td>${spec.cameraFeatures}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Video</td>
                                                <td>${spec.cameraVideo}</td>
                                            </tr>
                                            <tr class="th_title">

                                                <th colspan="2">
                                                    Processor</th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>CPU</td>
                                                <td>${spec.platformCpu}</td>
                                            </tr>

                                            <tr class="tr_el">
                                                <td>GPU</td>
                                                <td>${spec.platformGpu}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Chipset</td>
                                                <td>${spec.platformChipset}</td>
                                            </tr>
                                            <tr class="th_title">
                                                <th colspan="2">
                                                    Body </th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>body Dimensions</td>
                                                <td>${spec.bodyDimensions}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>body Weight</td>
                                                <td>${spec.bodyWeight}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>body Build</td>
                                                <td>${spec.bodyBuild}</td>
                                           </tr>
                                            <tr class="tr_el">
                                                <td>body Sim</td>
                                                <td>${spec.bodySim}</td>
                                            </tr>
                                            <tr class="th_title">
                                                <th colspan="2">
                                                    Other Features  </th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Sensors</td>
                                                <td>${spec.featuresSensors}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Messaging</td>
                                                <td>${spec.featuresMessaging}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Browser</td>
                                                <td>${spec.featuresBrowser}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>Java</td>
                                                <td>${spec.featuresJava}</td>
                                            </tr>
                                            <tr class="tr_el">
                                                <td> misc Colors</td>
                                                <td>${spec.miscColors}</td>
                                            </tr>

                                            </tr>
                                            <tr class="th_title">
                                                <th colspan="2">
                                                    Battery  </th>
                                            </tr>
                                            <tr class="tr_el">
                                                <td>battery specification</td>
                                                <td>${spec.battery}</td>
                                            </tr>
                                            
                                            
                                        </table>
                                    </div>


                                    <div class="tab-pane fade" id="companyprofile" >

                                        <c:forEach var="row" items="${requestScope.item.items}">
                                            <div class="col-sm-3">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products">
                                                        <div class="productinfo text-center">
                                                            <a href="<c:url value="/ItemDescription?id=${row.itemId}" />" >
                                                                <img src="${row.imageBase64Binary}" alt="" />
                                                            </a>
                                                            <h2>${row.itemPrice}</h2>
                                                            <p>${row.itemName}</p>
                                                            <a  href="CartShopController?action=addToCart&itemId=${row.itemId}&from=Home" class="btn btn-default add-to-cart" ><i class="fa fa-shopping-cart"></i>Add to cart</a>     
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>

                                    </div>
                                </div>
                            </div><!--/category-tab-->
                                
                        </c:if>
                    </div>
                </div>
            </div>
        </section>


        <%@include file="Footer.jsp" %>
        <%@include file="HomeHelper.jsp" %>

        <script src="js/jquery.js"></script>
        <script src="js/price-range.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>