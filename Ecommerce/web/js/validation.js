var group;

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function validateMobile(mobile) {
    var re = /^\d+$/;
    return re.test(mobile);
}

function OpenInNewTab(url) {
    var win = window.open(url, '_blank');

    win.document.write("first name: " + document.getElementById("firstname").value);
    win.document.write(" <br> last name: " + document.getElementById("lastname").value);
    win.document.write(" <br> Mobile: " + document.getElementById("m").value);
    win.document.write(" <br> Email: " + document.getElementById("email").value);
    win.document.write("<br> Interests: ");

    for (var i = 0; i < group.length; i++) {
        if (group[i].checked) {
            win.document.write(group[i].value);
            break;
        }
    }


    win.focus();

}

function validateForm()
{
    var valid = true;
    if (document.getElementById("firstname").value == "")
    {
        document.getElementById("fname").innerHTML = "*";
        document.getElementById("fname").style.color = "red";
        document.getElementById("firstname").focus();
        valid = false;
    } else {
        document.getElementById("fname").innerHTML = "";
    }

    if (document.getElementById("pass").value == "")
    {
        document.getElementById("pa").innerHTML = "*";
        document.getElementById("pa").style.color = "red";
        document.getElementById("pass").focus();
        valid = false;
    } else {
        document.getElementById("pa").innerHTML = "";
    }

    if (document.getElementById("lastname").value == "")
    {
        document.getElementById("lname").innerHTML = "*";
        document.getElementById("lname").style.color = "red";
        document.getElementById("lastname").focus();
        valid = false;
    } else {
        document.getElementById("lname").innerHTML = "";
    }


    var job = document.getElementById("m").value;
    if (job == "") {
        document.getElementById("lm").innerHTML = "*";
        document.getElementById("lm").style.color = "red";
        document.getElementById("m").focus();
        valid=false;
    }
    var country = document.getElementById("country").value
    if (country == -1) {
        document.getElementById("cou").innerHTML = "*";
        document.getElementById("cou").style.color = "red";
        document.getElementById("cou").focus();
        valid=false;
    } else {
        
        document.getElementById("cou").innerHTML = "";
        
    }
    var city = document.getElementById("state").value
    if (city == "") {
        document.getElementById("lci").innerHTML = "*";
        document.getElementById("lci").style.color = "red";
        document.getElementById("lci").focus();
        valid=false;
    } else {
        document.getElementById("lci").innerHTML = "";
        
    }
    if (document.getElementById("email").value == "")
    {
        document.getElementById("lemail").innerHTML = "*";
        document.getElementById("lemail").style.color = "red";
        document.getElementById("email").focus();
        valid = false;
    } else {
        var valide = validateEmail(document.getElementById("email").value);
        if (valide == false) {
            document.getElementById("lemail").innerHTML = "invalid Email";
            document.getElementById("lemail").style.color = "red";
            valid = false;
        } else {
            document.getElementById("lemail").innerHTML = "";
        }

    }

    return valid;
}

function validateAccount() {
    var valid = true;

    if (document.getElementById("CardNumber").value === "")
    {
        document.getElementById("CardNumberNoValue").innerHTML = "*";
        document.getElementById("CardValidationtError").innerHTML = "";
        document.getElementById("CardNumberNoValue").style.color = "red";
        document.getElementById("CardNumber").focus();
        valid = false;
    } 
    else if(isNaN(document.getElementById("CardNumber").value )){
        document.getElementById("CardValidationtError").innerHTML = "Invalid Card Number";
        document.getElementById("CardNumberNoValue").innerHTML = "";
        valid = false;
    }
    else {
        document.getElementById("CardNumberNoValue").innerHTML = "";
    }
    return valid;
}