
var flag = false;

function get(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
}




function chekCartState() {

    vex.dialog.alert('Item successfully added');
}

function mustLogin() {
    vex.dialog.alert('Sorry you must login first');
}

var usr;
function login() {
    vex.dialog.open({
        message: 'Enter your username and password:',
        input: "<input name=\"username\" type=\"text\" placeholder=\"Username\" required />\n<input name=\"password\" type=\"password\" placeholder=\"Password\" required />",
        buttons: [
            $.extend({}, vex.dialog.buttons.YES, {
                text: 'Login'
            }), $.extend({}, vex.dialog.buttons.NO, {
                text: 'Back'
            })
        ],
        callback: function (data) {
            if (data === false) {
                return console.log('Cancelled');
            }

            post("LoginController", data.username, data.password, "post");
        }
    });
}

function post(path, params1, params2, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "usrName");
    hiddenField1.setAttribute("value", params1);

    form.appendChild(hiddenField1);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "passwrd");
    hiddenField2.setAttribute("value", params2);

    form.appendChild(hiddenField2);

    document.body.appendChild(form);
    form.submit();
}

function  congrats(){
   vex.dialog.alert('Your credit successfully updated');
}

function invalidLogin(){
    vex.dialog.open({
        message: 'Enter your username and password:',
        input: "<style>.wrong{color:red;}</style><div class='wrong'>Invalid username or password</div><input name=\"username\" type=\"text\" placeholder=\"Username\" required />\n<input name=\"password\" type=\"password\" placeholder=\"Password\" required />",
        
        buttons: [
            $.extend({}, vex.dialog.buttons.YES, {
                text: 'Login'
            }), $.extend({}, vex.dialog.buttons.NO, {
                text: 'Back'
            })
        ],
        callback: function (data) {
            if (data === false) {
                return console.log('Cancelled');
            }

            post("LoginController", data.username, data.password, "post");
        }
    });
}