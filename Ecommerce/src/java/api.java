/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import iti36.ecommerce.mapping.CartShop;
import iti36.ecommerce.mapping.History;
import iti36.ecommerce.mapping.Item;
import iti36.ecommerce.mapping.Users;
import java.util.List;

/**
 *
 * @author adel
 */
public interface api {

    public Users logIn(Users user);

    public Users signUp(Users user);
    
    public Users checkOut(Users user, int totalPrice);
    
    public List<Users> getUsers();
    
    public List<History> getUserHistoryByUserId(Users user);
    
    public List<CartShop> getUserCartShopByUserId(Users user);
  
    public Users  cartOperation(Users user,int itemId ,String type);
   
}
