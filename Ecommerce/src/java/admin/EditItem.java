/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import iti36.ecommerce.dao.imp.DatabaseHandlerImp;
//import iti36.ecommerce.entity.Brand;
//import iti36.ecommerce.entity.Item;
//import iti36.ecommerce.entity.Specifications;
import iti36.ecommerce.mapping.Brand;
import iti36.ecommerce.mapping.Item;
import iti36.ecommerce.mapping.Specifications;
import iti36.ecommerce.util.ImageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import shika.DbHandlerHibernate;

/**
 *
 * @author user
 */
public class EditItem extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditItem</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditItem at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean status= false; 
//        Item item = DatabaseHandlerImp.getInstance().getItemForCart(Integer.parseInt(request.getParameter("itemId")));
//        Brand brand =DatabaseHandlerImp.getInstance().getBrandName(Integer.parseInt(request.getParameter("itemId")));
//        String att= request.getParameter("type");
//        String mobile= "Mobile";
//        if (att.equals(mobile))
//        {
//            Specifications sp =DatabaseHandlerImp.getInstance().getSpecByItemId(Integer.parseInt(request.getParameter("itemId")));
//            request.setAttribute("Sp", sp);
//            status = true;
//       }
//        request.setAttribute("editItem", item);
//        request.setAttribute("brand", brand);
//        request.setAttribute("status", status);

        Item item = new DbHandlerHibernate().getItemForUpdate(Integer.parseInt(request.getParameter("itemId")));
        item.setImageBase64Binary(ImageUtil.resizeImage(item.getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        if (request.getParameter("type").equals("Mobile"))
        {
            request.setAttribute("Sp", item.getSpecifications());
            status = true;
        }
        request.setAttribute("editItem", item);
        request.setAttribute("brand", item.getBrand());
        request.setAttribute("status", status);
        
        
        RequestDispatcher rd = request.getRequestDispatcher("/admin.jsp/edit.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
