/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import iti36.ecommerce.dao.imp.hibernate.DbHandlerHibernate;
import iti36.ecommerce.dao.imp.DatabaseHandlerAdminImp;
import iti36.ecommerce.mapping.CartShop;
import iti36.ecommerce.mapping.History;
import iti36.ecommerce.mapping.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yoka
 */
public class UserAdmin2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserAdmin2</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserAdmin2 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         int uId=Integer.parseInt(request.getParameter("uId"));
       int type=Integer.parseInt(request.getParameter("type"));
       String username=DatabaseHandlerAdminImp.getInstance().getUserByID(uId);
       request.setAttribute("userName", username);
       Users user = new Users();
       user.setId(uId);
        switch (type) {
            case 1:
               List<History> arr_his= new DbHandlerHibernate().getUserHistoryByUserId(user);
               request.setAttribute("history", arr_his);
               request.getRequestDispatcher("/admin.jsp/Userhistory.jsp").forward(request, response);
                break;
            case 2:
                List<CartShop> arr_cart= new DbHandlerHibernate().getUserCartShopByUserId(user);
                System.out.println("size :"+arr_cart.size());
                request.setAttribute("cartshop", arr_cart);
               request.getRequestDispatcher("/admin.jsp/UserCartShop.jsp").forward(request, response);
                break;
            default:
                response.sendRedirect("error.html");
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
