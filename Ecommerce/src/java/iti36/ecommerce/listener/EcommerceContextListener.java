package iti36.ecommerce.listener;

import iti36.ecommerce.dao.imp.hibernate.DbHandlerHibernate;
import iti36.ecommerce.mapping.Brand;
import java.util.Vector;
import javax.servlet.*;

public class EcommerceContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent scEvent) {
        ServletContext servletContext = scEvent.getServletContext();
        Vector<Brand> brand =  new DbHandlerHibernate().getAllItemBrand();
        Vector<String> accessorie = new DbHandlerHibernate().getAllAccessorieType();
        servletContext.setAttribute("brand", brand);
        servletContext.setAttribute("accessorie", accessorie);
    }

    @Override
    public void contextDestroyed(ServletContextEvent scEvent) {

    }
}
