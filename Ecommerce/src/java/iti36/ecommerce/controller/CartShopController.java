/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.controller;

import iti36.ecommerce.dao.imp.hibernate.DbHandlerHibernate;
import iti36.ecommerce.dao.imp.DatabaseHandlerImp;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import iti36.ecommerce.mapping.CartShop;
import iti36.ecommerce.mapping.Users;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wafik Aziz
 */
public class CartShopController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession userSession = request.getSession(false);
        int userId = 0;
        String from = request.getParameter("from");
        System.out.println("from : " + from);
        if (userSession != null) {
            if (userSession.getAttribute("loginUser") != null) {
                Users user = (Users) userSession.getAttribute("loginUser");
                userId = user.getId();
                String action = request.getParameter("action");
                String ItemId = request.getParameter("itemId");
                String indx = request.getParameter("indx");
                Set<CartShop> cartShopSet = (Set<CartShop>) user.getCartShops();
                List<CartShop> ob = new ArrayList<>();
                Iterator<CartShop> cartShopIterator = cartShopSet.iterator();
                while (cartShopIterator.hasNext()) {
                    CartShop cartShop = cartShopIterator.next();
                    ob.add(cartShop);

                }
                DatabaseHandlerImp dbHand = DatabaseHandlerImp.getInstance();
                request.setAttribute("cartObjcts", ob);
                request.setAttribute("cnt", ob.size());
                if ("add".equals(action)) {
                    // dbHand.addCartItemQuantity(userId, ItemId, Integer.parseInt(indx), cartShp);
                    //  user = updateUserSession(userId, userSession);
                    user = new DbHandlerHibernate().cartOperation(user, Integer.parseInt(ItemId), "plus");
                    userSession.setAttribute("loginUser", user);
                    response.sendRedirect("CartShopController");
                } else if ("sub".equals(action)) {
                    //dbHand.subCartItemQuantity(userId, ItemId, Integer.parseInt(indx), cartShp);
                    //user = updateUserSession(userId, userSession);
                    user = new DbHandlerHibernate().cartOperation(user, Integer.parseInt(ItemId), "sub");
                    userSession.setAttribute("loginUser", user);
                    response.sendRedirect("CartShopController");
                } else if ("del".equals(action)) {
                    dbHand.deleteCartItem(userId, ItemId);
                    //user = updateUserSession(userId, userSession);
                    user = new DbHandlerHibernate().cartOperation(user, Integer.parseInt(ItemId), "delete");
                    userSession.setAttribute("loginUser", user);
                    response.sendRedirect("CartShopController");
                } else if (("addToCart").equals(action)) {
                    user  = new DbHandlerHibernate().cartOperation(user,Integer.parseInt(ItemId),"addToCart");
                    System.out.println("iti36.ecommerce.controller.CartShopController.doGet()" + user);
                    userSession.setAttribute("loginUser", user);
                    userSession.setAttribute("from", "cart");
                    if ("Home".equals(from)) {
                        response.sendRedirect("HomeController");
                    } else if ("shop".equals(from)) {
                        response.sendRedirect("ShopController");
                    }
                } else {
                    RequestDispatcher rd = request.getRequestDispatcher("/jsp/CartJSP.jsp");
                    rd.forward(request, response);
                }
            } else {
                userSession.setAttribute("from", "cart");
                if ("Home".equals(from)) {
                    response.sendRedirect("HomeController");
                } else if ("shop".equals(from)) {
                    response.sendRedirect("ShopController");
                } else {
                    response.sendRedirect("HomeController");
                }
            }
        } else {
            response.sendRedirect("HomeController");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

//    private User updateUserSession(int userId, HttpSession userSession) {
////        User users = (User) userSession.getAttribute("loginUser");
////        ArrayList<CartShop> cart = DatabaseHandlerImp.getInstance().getUserCartItems(userId);
////        users.setCartShopList(null);
////        users.setCartShopList(cart);
////        userSession.setAttribute("loginUser", users);
//        return users;
//    }

    private int  findItem(int itemId, Set<CartShop> cart) {
        int found = -1;
        for (int i =0 ; i<cart.size();i++) {
//            if (cart..getItemId() == itemId) {
//                found = i;
//                break;
//            }
        }
        return found;
    }
}
