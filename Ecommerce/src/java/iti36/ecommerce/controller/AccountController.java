/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.controller;

import iti36.ecommerce.dao.imp.hibernate.DbHandlerHibernate;
import iti36.ecommerce.mapping.Users;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wafik Aziz
 */
public class AccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AccountController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AccountController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cardNumber = request.getParameter("CardNumber");
//        boolean isCardVaild = DatabaseHandlerImp.getInstance().checkcardNumber(Integer.parseInt(cardNumber));
        boolean isCardVaild = new DbHandlerHibernate().checkcardNumber(Integer.parseInt(cardNumber));
        if (isCardVaild == false) {
            request.setAttribute("CardValidationtError", "Invalid Card Number");
            getServletContext().getRequestDispatcher("/jsp/Account.jsp").forward(request, response);
        } else {
            HttpSession userSession = request.getSession(false);
//           User user = (User) userSession.getAttribute("loginUser");
//           User UpdatedUser = DatabaseHandlerImp.getInstance().updateUserBalance(Integer.parseInt(cardNumber), user);
            Users user = (Users) userSession.getAttribute("loginUser");
            Users UpdatedUser = new DbHandlerHibernate().updateUserBalance(Integer.parseInt(cardNumber), user);
            userSession.setAttribute("loginUser", UpdatedUser);
            userSession.setAttribute("to", "Account");
            response.sendRedirect("HomeController");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
