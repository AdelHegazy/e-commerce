/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.util;

import iti36.ecommerce.entity.Item;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 * @author adel
 */
public class ItemDecoder {

    private Cipher desCipher;
    private SecretKey myDesKey;
    private KeyGenerator keygenerator;
    private static ItemDecoder instance;

    public static synchronized ItemDecoder getInstance() {
        if (instance == null) {
            instance = new ItemDecoder() {
            };
        }
        return instance;
    }

    private ItemDecoder() {
        keygenerator();
    }

    private void keygenerator() {
        try {
            keygenerator = KeyGenerator.getInstance("DES");
            myDesKey = keygenerator.generateKey();
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException ex) {
            Logger.getLogger(ItemDecoder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void encode(Item item) {

        try {
            byte[] text = String.valueOf(item.getItemId()).getBytes("UTF-8");
            byte[] textEncrypted = desCipher.doFinal(text);
            String code = new String(textEncrypted);
            item.setItemIdEncode(code);

        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(ItemDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ItemDecoder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int decode(String id) {
        byte[] textDecrypted;
        try {
            desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
            textDecrypted = desCipher.doFinal(id.getBytes());
            return Integer.parseInt(new String(textDecrypted, "UTF-8"));
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(ItemDecoder.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ItemDecoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public void resetKey() {
        instance = null;
    }
}
