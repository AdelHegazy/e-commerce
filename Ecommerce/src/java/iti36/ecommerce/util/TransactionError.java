/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.util;

import iti36.ecommerce.entity.Item;

/**
 *
 * @author adel
 */
public class TransactionError {
    private static  int itemError = -1 ;

    public static int getItemError() {
        return itemError;
    }

    public static void setItemError(int itemError) {
        TransactionError.itemError = itemError;
    }
 
}
