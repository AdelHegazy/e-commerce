/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.util;

import iti36.ecommerce.dao.imp.DatabaseHandlerImp;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import sun.misc.BASE64Encoder;
import java.io.InputStream;

/**
 *
 * @author adel
 */
public class ImageUtil {

    private final static String JPG = "jpg";
    private final static String IMAGE_BASE = "data:image/jpg;base64,";
    public static final int WIDTH = 268;
    public static final int HIGHT = 249;

    public static byte[] read(Blob img) {
        byte[] imgData = null;
        try {
            imgData = img.getBytes(1, (int) img.length());
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imgData;
    }

    public static String resizeImage(byte[] img, int width, int height) {
        String imageUri = IMAGE_BASE;
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(img);
            BufferedImage Bufimg = ImageIO.read(in);
            Image scaledImage = Bufimg.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            ImageIO.write(imageBuff, JPG, buffer);
            BASE64Encoder encoder = new BASE64Encoder();
            imageUri = imageUri + encoder.encode(buffer.toByteArray());
        } catch (IOException ex) {
            Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imageUri;
    }

        public static InputStream write(String imgData) {
            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(new File(imgData));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ImageUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
            return inputStream;
        }

}
