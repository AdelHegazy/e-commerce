/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.api.hibernate;

import iti36.ecommerce.mapping.Brand;
import iti36.ecommerce.mapping.CartShop;
import iti36.ecommerce.mapping.History;
import iti36.ecommerce.mapping.Item;
import iti36.ecommerce.mapping.Specifications;
import iti36.ecommerce.mapping.Users;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author adel
 */
public interface DbHandlerHibernateInterface {

    public Users logIn(Users user);

    public Users signUp(Users user);

    public Users checkOut(Users user, int totalPrice);

    public List<Users> getUsers();

    public List<History> getUserHistoryByUserId(Users user);

    public List<CartShop> getUserCartShopByUserId(Users user);

    public Users cartOperation(Users user, int itemId, String type);

    public Users AddCartItem(Users user, String itemid);

    public Vector<Brand> getAllItemBrand();

    public Vector<String> getAllAccessorieType();

    public Vector<Item> findItemsByExample(Item item, int low, int high);

    public List<Item> getAllItems(int from, int to);

    public boolean checkcardNumber(int cardNumber);

    public Users updateUserBalance(int cardNumber, Users user);

    public Vector<Item> getHomeItems(); // return 15 items Des

    public Item getItemById(int pId);

    public Specifications getSpecByItemId(int pId);

    public String getItemLength();

    public void addItem(Item item);
}
