/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.api;

import iti36.ecommerce.entity.Brand;
import iti36.ecommerce.entity.Item;
import iti36.ecommerce.entity.Specifications;
import java.io.FileInputStream;

/**
 *
 * @author Shika
 */
public interface DatabaseHandlerAdminInterface {
    public void addSpecifications(Item item,int itemID);
    public void addBrand(Brand brand);
    public void addItem(Item item);
    public void deleteItem(Item item);
    public void changeItem(Item item,Specifications sp);
}
