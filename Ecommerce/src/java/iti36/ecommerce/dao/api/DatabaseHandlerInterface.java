/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.api;

import iti36.ecommerce.entity.Brand;
import iti36.ecommerce.entity.CartShop;
import iti36.ecommerce.entity.History;
import java.util.Vector;
import iti36.ecommerce.entity.Item;
import iti36.ecommerce.entity.User;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shika
 */
public interface DatabaseHandlerInterface {

    public Vector<Item> getAllItems();

    public Vector<Item> getHomeItems();

    public int getCartItemCount();

    public ArrayList<CartShop> getUserCartItems(int id);

    public Item getItemForCart(int itemId);

    public void addCartItemQuantity(int userId, String itemId, int index, ArrayList<CartShop> cartShp);

    public void subCartItemQuantity(int userId, String itemId, int index, ArrayList<CartShop> cartShp);

    public void deleteCartItem(int userId, String itemId);

    public Vector<Brand> getAllItemBrand();

    public Vector<Item> searchBrand(int BrandnameId);

    public User signUp(User user);

    public Vector<Item> searchBrandIdAndType(int BrandId, String type);

    public Vector<Item> searchBrandIdAndTypeAndPrice(int BrandId, String type, int minPrice, int maxPrice);

    public Vector<Item> searchWithItemTypeAndPrice(String type, int minPrice, int maxPrice);

    public Vector<String> getAllAccessorieType();

    public boolean AddCartItem(int userid, String itemid);

    public Vector<Item> getAllAccessories(Item item, int minPrice, int maxPrice);   
    
    public User logIn(User user);

    public User getUser(ResultSet result);

    public CartShop getCartShop(ResultSet result);
    
    public User checkOut(User user,int totalPrice);

    public List<History> getUserHistory(int userId);
    
    public boolean checkCardNumber(int CardNumber);
    
    public int getCardBalance(int CardNumber);
    
    public void invalidateCard(int CardNumber);
    
    public User updateUserBalance(int CardNumber,User user);
}
