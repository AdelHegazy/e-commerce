package iti36.ecommerce.dao.imp;

import iti36.ecommerce.constatnt.BrandConstatnt;
import iti36.ecommerce.constatnt.CartShopConstant;
import iti36.ecommerce.constatnt.Constant;
import iti36.ecommerce.constatnt.HistoryConstatnt;
import iti36.ecommerce.entity.Item;
import iti36.ecommerce.constatnt.ItemConstant;
import iti36.ecommerce.constatnt.Query;
import iti36.ecommerce.constatnt.SpecConstant;
import iti36.ecommerce.constatnt.UserConstant;
import iti36.ecommerce.dao.api.DatabaseHandlerInterface;
import iti36.ecommerce.dao.util.DbOpenConnection;
import iti36.ecommerce.entity.Brand;
import iti36.ecommerce.util.ImageUtil;
import iti36.ecommerce.util.ItemDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import iti36.ecommerce.entity.CartShop;
import iti36.ecommerce.entity.History;
import iti36.ecommerce.entity.User;

import iti36.ecommerce.util.TransactionError;
import java.sql.Date;

import iti36.ecommerce.entity.Specifications;

import java.sql.PreparedStatement;
import java.util.List;

public class DatabaseHandlerImp implements DatabaseHandlerInterface {

    private static DatabaseHandlerImp instance;
    private java.util.Date today;

    public static synchronized DatabaseHandlerImp getInstance() {
        if (instance == null) {
            instance = new DatabaseHandlerImp() {
            };
        }
        return instance;
    }

    /**
     * @param void no param to set
     * @return vector of all items
     */
    @Override
    public Vector<Item> getAllItems() {
        ResultSet result = null;
        Vector<Item> items = new Vector<>();
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.GET_ALL_ITEMS);
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                items.add(getItem(result));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }

    /**
     * convert image blob from database to array of bytes
     *
     * @param Blob image
     * @return Bytes[] of all items
     */
    @Override
    public Vector<Item> getHomeItems() {
        Vector<Item> items = new Vector<>();
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.GET_HOME_ITEMS);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                items.add(getItem(result));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }

    private Item getItem(ResultSet result) {
        Item item = new Item();
        try {
            item.setItemId(result.getInt(ItemConstant.ITEM_ID));
            item.setItemName(result.getString(ItemConstant.ITEM_NAME));
            item.setItemQuantity(result.getInt(ItemConstant.ITEM_QUANTITY));
            item.setItemPrice(result.getInt(ItemConstant.ITEM_PRICE));
            item.setItemType(result.getString(ItemConstant.ITEM_TYPE));
            item.setImageBase64Binary(ImageUtil.resizeImage(ImageUtil.read(result.getBlob(ItemConstant.ITEM_IMAGE)), ImageUtil.WIDTH, ImageUtil.HIGHT));
            item.setItemDesc(result.getString(ItemConstant.ITEM_DESC));
            if (item.getItemType().equals(ItemConstant.MOBILE)) {
                item.setAccessorieType(result.getString(ItemConstant.ACCESSORIE_TYPE));
            }
            ItemDecoder.getInstance().encode(item);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return item;
    }

    @Override
    public int getCartItemCount() {
        ResultSet result = null;
        int count = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.CART_ITEM_COUNT);

            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                count = result.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count;
    }

    @Override
    public ArrayList<CartShop> getUserCartItems(int id) {
        ArrayList<CartShop> cartShp = new ArrayList<>();
        ResultSet result = null;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.CART_USERS_ITEMS);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, Integer.toString(id));
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();

            while (result.next()) {
                cartShp.add(getCartShop(result));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cartShp;
    }

    @Override
    public Item getItemForCart(int itemId) {
        ResultSet result = null;
        Item item = new Item();

        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.ONE_CART_ITEM);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, Integer.toString(itemId));
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                item = getItem(result);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    @Override
    public void addCartItemQuantity(int userId, String itemId, int index, ArrayList<CartShop> cartShp) {
        try {
            //  Item myItem = this.getItemForCart(Integer.parseInt(itemId));
            int oldQuantity = cartShp.get(index).getQuantity();
            int newQuantity = oldQuantity + 1;

            DbOpenConnection.getInstance().setPreparedStatement(Query.CHNG_CART_ITEM_QUANTITY);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, Integer.toString(newQuantity));
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, Integer.toString(userId));
            DbOpenConnection.getInstance().getPreparedStatement().setString(3, itemId);
            DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void subCartItemQuantity(int userId, String itemId, int index, ArrayList<CartShop> cartShp) {
        try {
            //  Item myItem = this.getItemForCart(Integer.parseInt(itemId));
            int oldQuantity = cartShp.get(index).getQuantity();
            int newQuantity = oldQuantity - 1;
            if (newQuantity > 0) {
                DbOpenConnection.getInstance().setPreparedStatement(Query.CHNG_CART_ITEM_QUANTITY);
                DbOpenConnection.getInstance().getPreparedStatement().setString(1, Integer.toString(newQuantity));
                DbOpenConnection.getInstance().getPreparedStatement().setString(2, Integer.toString(userId));
                DbOpenConnection.getInstance().getPreparedStatement().setString(3, itemId);
                DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteCartItem(int userId, String itemId) {
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.DELETE_CART_ITEM);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, itemId);
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, Integer.toString(userId));
            DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public User signUp(User user) {
        int effectedRow;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.SingUpQuary,new String[]{"id"});
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();
            ps.setString(1, user.getFirstName());
            ps.setString(2, user.getLastName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getJob());
            ps.setString(6, user.getCountry());
            ps.setString(7, user.getCity());
            ps.setString(8, user.getStreet());
            ps.setInt(9, user.getBalance());
            effectedRow = ps.executeUpdate();
            if (effectedRow != 0) {
                System.out.println("sign up done");
                try {
                    ResultSet generatedKey = ps.getGeneratedKeys();
                    if (generatedKey.next()) {
                        user.setId((int)generatedKey.getLong(1));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                user = null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    @Override
    public Vector<Item> searchBrand(int BrandId) {
        Vector<Item> items = new Vector<>();
        Item item = null;
        ResultSet result = null;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.searchwithBrandID);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();
            ps.setInt(1, BrandId);
            result = ps.executeQuery();
            while (result.next()) {
                item = new Item();
                items.add(getItem(result));
                System.out.println(items.get(i).getItemName());
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }

        return items;
    }

    @Override
    public Vector<Item> searchBrandIdAndType(int BrandId, String type) {
        Vector<Item> items = new Vector<>();
        Item item = null;
        ResultSet result = null;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.searchwithBrandIDandtype);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();
            ps.setInt(1, BrandId);
            ps.setString(2, type);
            result = ps.executeQuery();
            while (result.next()) {
                item = new Item();
                items.add(getItem(result));
                System.out.println(items.get(i).getItemName());
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }

    //used
    @Override
    public Vector<Item> searchBrandIdAndTypeAndPrice(int BrandId, String type, int minPrice, int maxPrice) {
        Vector<Item> items = new Vector<>();
        Item item = null;
        ResultSet result = null;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.searchwithBrandIDandtypeAndPrice);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();
            ps.setInt(1, BrandId);
            ps.setString(2, type);
            ps.setInt(3, minPrice);
            ps.setInt(4, maxPrice);
            result = ps.executeQuery();
            while (result.next()) {
                item = new Item();
                items.add(getItem(result));
                System.out.println(items.get(i).getItemName());
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;

    }

    @Override
    public boolean AddCartItem(int userid, String itemid) {
        CartShop crtShp = new CartShop();
        crtShp.setItem(this.getItemForCart(Integer.parseInt(itemid)));
        int result = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.ADD_CART_ITEM);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, Integer.toString(userid));
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, itemid);
            DbOpenConnection.getInstance().getPreparedStatement().setString(3, "1");
            DbOpenConnection.getInstance().getPreparedStatement().setString(4, Integer.toString(crtShp.getItem().getItemPrice()));
            result = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Vector<Item> searchWithItemTypeAndPrice(String type, int minPrice, int maxPrice) {
        Vector<Item> items = new Vector<>();
        Item item = null;
        ResultSet result = null;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.searchWithItemTypeAndPrice);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();
            //ps.setInt(1, BrandId);
            ps.setString(1, type);
            ps.setInt(2, minPrice);
            ps.setInt(3, maxPrice);
            result = ps.executeQuery();
            while (result.next()) {
                item = new Item();
                items.add(getItem(result));
                System.out.println(items.get(i).getItemName());
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
//used

    @Override
    public Vector<Brand> getAllItemBrand() {
        Vector<Brand> brands = new Vector<>();
        Brand brand;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.GET_ALL_BRAND);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                brand = new Brand();
                brand.setId(Integer.parseInt(result.getString(BrandConstatnt.BRAND_ID)));
                brand.setBrandName(result.getString(BrandConstatnt.Brand_NAME));
                brands.add(brand);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return brands;
    }
//used

    @Override
    public Vector<String> getAllAccessorieType() {
        Vector<String> accessorie = new Vector<>();
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.GET_ALL_Accessorie_TYPE);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                accessorie.add(result.getString(ItemConstant.ACCESSORIE_TYPE));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accessorie;
    }
//used

    @Override
    public Vector<Item> getAllAccessories(Item item, int minPrice, int maxPrice) {
        Vector<Item> items = new Vector<>();
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.GET_ALL_ACCESSORIE);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, item.getAccessorieType());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(2, minPrice);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(3, maxPrice);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                items.add(getItem(result));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;

    }

    // used
    @Override
    public User logIn(User user) {
        int rowNo = 0;
        ResultSet result = null;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.COUNT);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getPassword());
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                rowNo = result.getInt(1);
            }
            if (rowNo == 1) {
                DbOpenConnection.getInstance().setPreparedStatement(Query.LOG_IN);
                DbOpenConnection.getInstance().getPreparedStatement().setString(1, user.getEmail());
                DbOpenConnection.getInstance().getPreparedStatement().setString(2, user.getPassword());
                result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
                user = getUser(result);
                user.setHistoryList(getUserHistory(user.getId()));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rowNo == 0) {
            return null;
        } else {
            return user;
        }
    }
// used

    @Override
    public User getUser(ResultSet result) {
        User user = new User();
        ArrayList<CartShop> cartShopList = new ArrayList<>();
        CartShop cartShop = new CartShop();
        try {
            result.next();
            user.setId(result.getInt(UserConstant.ID));
            user.setFirstName(result.getString(UserConstant.FIRST_NAME));
            user.setLastName(result.getString(UserConstant.LAST_NAME));
            user.setEmail(result.getString(UserConstant.EMAIL));
            user.setPassword(result.getString(UserConstant.PASSWORD));
            user.setJob(result.getString(UserConstant.JOB));
            user.setBalance(result.getInt(UserConstant.BALANCE));
            user.setCity(result.getString(UserConstant.CITY));
            user.setCountry(result.getString(UserConstant.COUNTRY));
            user.setStreet(result.getString(UserConstant.STREET));
            DbOpenConnection.getInstance().setPreparedStatement(Query.CART_USERS_ITEMS);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, user.getId());
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                cartShop = getCartShop(result);
                cartShopList.add(cartShop);
            }
            user.setCartShopList(cartShopList);
            for (int i = 0; i < user.getCartShopList().size(); i++) {
                user.getCartShopList().get(i).setItem(getItemForCart(user.getCartShopList().get(i).getItemId()));
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    // used
    @Override
    public CartShop getCartShop(ResultSet result) {
        CartShop crtShp = new CartShop();
        try {
            crtShp.setUserId(result.getInt(CartShopConstant.USER_ID));
            crtShp.setItemId(result.getInt(CartShopConstant.ITEM_ID));
            crtShp.setQuantity(result.getInt(CartShopConstant.QUANTITY));
            crtShp.setPrice(result.getInt(CartShopConstant.PRICE));
            crtShp.setItem(getItemForCart(crtShp.getItemId()));
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return crtShp;
    }

    @Override
    public User checkOut(User user, int totalPrice) {
        try {
            if (validateTransaction(user, totalPrice)) {
                user.getCartShopList().clear();
                user.setHistoryList(getUserHistory(user.getId()));
                user.setBalance(user.getBalance() - totalPrice);
                DbOpenConnection.getInstance().getConnection().commit();
            } else {
                DbOpenConnection.getInstance().getConnection().rollback();
            }
            DbOpenConnection.getInstance().getConnection().setAutoCommit(true);
        } catch (Exception ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    // check on item quantity and update item quantity if its valid
    // inset in history 
    // remove from cart
    public boolean validateTransaction(User user, int total) {
        try {
            DbOpenConnection.getInstance().getConnection().setAutoCommit(false);
            for (CartShop singleCart : user.getCartShopList()) {
                DbOpenConnection.getInstance().setPreparedStatement(Query.GET_ONE_ITEM);
                DbOpenConnection.getInstance().getPreparedStatement().setInt(1, singleCart.getItemId());
                ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
                result.next();
                int itemQuantity = Integer.parseInt(result.getString(ItemConstant.ITEM_QUANTITY));
                int price = result.getInt(ItemConstant.ITEM_PRICE);
                if (itemQuantity < singleCart.getQuantity()) {
                    TransactionError.setItemError(singleCart.getItemId());
                    return false;
                } else {
                    int newQuantity = itemQuantity - singleCart.getQuantity();
                    DbOpenConnection.getInstance().setPreparedStatement(Query.CHANGE_ITEM_QUANTITY);
                    DbOpenConnection.getInstance().getPreparedStatement().setInt(1, newQuantity);
                    DbOpenConnection.getInstance().getPreparedStatement().setInt(2, singleCart.getItemId());
                    int update = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
                    if (update != 0) {
                        DbOpenConnection.getInstance().setPreparedStatement(Query.ADD_ITEM_HISTORY);
                        DbOpenConnection.getInstance().getPreparedStatement().setInt(1, singleCart.getUserId());
                        DbOpenConnection.getInstance().getPreparedStatement().setInt(2, singleCart.getItemId());
                        DbOpenConnection.getInstance().getPreparedStatement().setInt(3, singleCart.getQuantity());
                        DbOpenConnection.getInstance().getPreparedStatement().setInt(4, price);
                        DbOpenConnection.getInstance().getPreparedStatement().setDate(5, getCurrentDate());
                        if (DbOpenConnection.getInstance().getPreparedStatement().executeUpdate() != 0) {
                            DbOpenConnection.getInstance().setPreparedStatement(Query.DELETE_CART_SHOP_TO_USER);
                            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, singleCart.getItemId());
                            DbOpenConnection.getInstance().getPreparedStatement().setInt(2, singleCart.getUserId());
                            int delete = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
                            if (delete == 0) {
                                TransactionError.setItemError(singleCart.getItemId());
                                DbOpenConnection.getInstance().getConnection().rollback();
                                return false;
                            }
                        } else {
                            TransactionError.setItemError(singleCart.getItemId());
                            DbOpenConnection.getInstance().getConnection().rollback();
                            return false;
                        }
                    } else {
                        TransactionError.setItemError(singleCart.getItemId());
                        DbOpenConnection.getInstance().getConnection().rollback();
                        return false;
                    }
                }
            }
            DbOpenConnection.getInstance().setPreparedStatement(Query.UPDATE_USER_BALANCE);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, user.getBalance() - total);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(2, user.getId());
            if (DbOpenConnection.getInstance().getPreparedStatement().executeUpdate() == 0) {
                DbOpenConnection.getInstance().getConnection().rollback();
            }
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }

    private Date getCurrentDate() {
        today = new java.util.Date();
        return new Date(today.getTime());
    }

    @Override
    public List<History> getUserHistory(int userId){
        List<History> historyList = new ArrayList<>();
        try {
            History history;
            DbOpenConnection.getInstance().setPreparedStatement(Query.GET_USER_ITEM_HISTORY);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, userId);
            ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                history = new History();
                history.setId(result.getInt(HistoryConstatnt.ID));
                history.setPrice(result.getInt(HistoryConstatnt.PRICE));
                history.setQuantity(result.getInt(HistoryConstatnt.QUANTITY));
                history.setDate(result.getDate(HistoryConstatnt.Date));
                history.setItem(getItemForCart(result.getInt(HistoryConstatnt.ITEM_ID)));
                historyList.add(history);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return historyList;
    }

    public Item getItemById(int pId) {

        Item item = null;
        ResultSet result;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.GET_ITEM_BY_ID);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();

            ps.setInt(1, pId);

            result = ps.executeQuery();
            while (result.next()) {
                item = getItemByBrand(result);
                // System.out.println(items.get(i).getItemName());          
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return item;

    }

    public Specifications getSpecByItemId(int pId) {
        Specifications spec = null;
        ResultSet result;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.GET_SPEC_BY_ITEM_ID);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();

            ps.setInt(1, pId);

            result = ps.executeQuery();
            while (result.next()) {
                spec = new Specifications();
                spec.setId(result.getInt(SpecConstant.spec_id));
                Item it = new Item();
                it.setItemId(result.getInt(SpecConstant.item_id));
                spec.setBatteryStandby(result.getString(SpecConstant.battery_standby));
                spec.setBatteryTalktime(result.getString(SpecConstant.battery_talktime));
                spec.setBodyBuild(result.getString(SpecConstant.body_build));
                spec.setBodyDimensions(result.getString(SpecConstant.body_dim));
                spec.setBodyWeight(result.getString(SpecConstant.body_weight));
                spec.setSpecItemId(it);
                spec.setBattery(result.getString(SpecConstant.battery));
                spec.setBodySim(result.getString(SpecConstant.body_sim));
                spec.setCameraFeatures(result.getString(SpecConstant.camera_features));
                spec.setCameraPrimary(result.getString(SpecConstant.camera_primary));
                spec.setCameraSecondary(result.getString(SpecConstant.camera_sec));
                spec.setCameraVideo(result.getString(SpecConstant.camera_video));
                spec.setCommsBluetooth(result.getString(SpecConstant.bluetooth));
                spec.setCommsRadio(result.getString(SpecConstant.radio));
                spec.setCommsGps(result.getString(SpecConstant.gps));
                spec.setCommsNfc(result.getString(SpecConstant.nfs));
                spec.setCommsUsb(result.getString(SpecConstant.usb));
                spec.setCommsWlan(result.getString(SpecConstant.wlan));
                spec.setDisplayMultitouch(result.getString(SpecConstant.display_touch));
                spec.setDisplayProtection(result.getString(SpecConstant.display_protect));
                spec.setDisplayResolution(result.getString(SpecConstant.display_res));
                spec.setDisplaySize(result.getString(SpecConstant.display_size));
                spec.setDisplayType(result.getString(SpecConstant.display_type));
                spec.setFeaturesBrowser(result.getString(SpecConstant.browser));
                spec.setFeaturesJava(result.getString(SpecConstant.java));
                spec.setFeaturesMessaging(result.getString(SpecConstant.msg));
                spec.setFeaturesSensors(result.getString(SpecConstant.sensor));
                spec.setLaunchAnnounced(result.getString(SpecConstant.launched));
                spec.setMemoryCardslot(result.getString(SpecConstant.card_slot));
                spec.setMemoryInternal(result.getString(SpecConstant.mem_internal));
                spec.setMiscColors(result.getString(SpecConstant.misc));
                spec.setMmJack(result.getString(SpecConstant.mm_jack));
                spec.setNetworkTechnology(result.getString(SpecConstant.net_tech));
                spec.setPlatformChipset(result.getString(SpecConstant.chipset));
                spec.setPlatformCpu(result.getString(SpecConstant.cpu));
                //spec.set
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return spec;
    }

    private Item getItemByBrand(ResultSet result) {
        Item item = new Item();
        try {
            int id=result.getInt(ItemConstant.ITEM_ID);
            item.setItemId(id);
            item.setItemName(result.getString(ItemConstant.ITEM_NAME));
            item.setItemQuantity(result.getInt(ItemConstant.ITEM_QUANTITY));
            item.setItemPrice(result.getInt(ItemConstant.ITEM_PRICE));
            item.setItemType(result.getString(ItemConstant.ITEM_TYPE));
            item.setImageBase64Binary(ImageUtil.resizeImage(ImageUtil.read(result.getBlob(ItemConstant.ITEM_IMAGE)), ImageUtil.WIDTH, ImageUtil.HIGHT));
            item.setItemDesc(result.getString(ItemConstant.ITEM_DESC));
            Brand b = new Brand();
            b.setBrandName(result.getString(ItemConstant.BRAND_NAME));
            item.setItemBrandId(b);
            List<Item> lit=get_recc_items(id);
           for(int i=0;i<lit.size();i++)
           { item.getItemList().add(lit.get(i));}
            if (item.getItemType().equals(ItemConstant.MOBILE)) {
                item.setAccessorieType(result.getString(ItemConstant.ACCESSORIE_TYPE));
            }
            ItemDecoder.getInstance().encode(item);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return item;
    }

    private List<Item> get_recc_items(int item_id) {
        List<Item> litems = null;
        ResultSet result;
        int i = 0;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Constant.GET_RECC_ITEMS_BY_ITEM_ID);
            PreparedStatement ps = DbOpenConnection.getInstance().getPreparedStatement();

            ps.setInt(1, item_id);

            result = ps.executeQuery();
           litems = new ArrayList<>();
            while (result.next()) {
                
            litems.add(getItem(result));
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return litems;
    }
    public Brand getBrandName(int itemId)
    {  Brand brand = new Brand() ;
         ResultSet result = null;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.getBrandName);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, itemId);
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                brand.setBrandName(result.getString("brand-name"));            
                brand.setId(result.getInt("id")); 
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    return brand ;
    }

    @Override
    public boolean checkCardNumber(int CardNumber) {

        ResultSet result = null;
        boolean isCardVaild = false;
        int valid = 0;

        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.CARD);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, CardNumber);
            
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                valid = result.getInt(3);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (valid == 1) {
            isCardVaild = true;
        }
        return isCardVaild;
    }
    
    @Override
    public int getCardBalance(int CardNumber){
        ResultSet result = null;
        int Balance = 0;
        
         try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.CARD);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, CardNumber);
            
            result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (result.next()) {
                Balance = result.getInt(2);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return Balance;
    }
    
    @Override
    public void invalidateCard(int CardNumber){
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.INVALIDATE_CARD);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, CardNumber);
            DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public User updateUserBalance(int CardNumber,User user){
       
        int balance = this.getCardBalance(CardNumber);
        int newBalance = user.getBalance()+balance;
        user.setBalance(newBalance);
        this.invalidateCard(CardNumber);
        
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.UPDATE_USER_BALANCE );
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, newBalance);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(2, user.getId());
            DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        
        return user;
    }

}
