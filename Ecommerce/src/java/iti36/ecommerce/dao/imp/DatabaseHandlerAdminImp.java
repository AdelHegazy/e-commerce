/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.imp;

import iti36.ecommerce.constatnt.CartShopConstant;
import iti36.ecommerce.constatnt.ItemConstant;
import iti36.ecommerce.constatnt.Query;
import iti36.ecommerce.constatnt.UserConstant;
import iti36.ecommerce.dao.api.DatabaseHandlerAdminInterface;
import iti36.ecommerce.dao.util.DbOpenConnection;
import iti36.ecommerce.dao.util.DbOpenSession;
import iti36.ecommerce.entity.Brand;
import iti36.ecommerce.entity.CartShop;
import iti36.ecommerce.entity.Item;
import iti36.ecommerce.entity.Specifications;
import iti36.ecommerce.entity.User;
import iti36.ecommerce.util.ImageUtil;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author Shika
 */
public class DatabaseHandlerAdminImp implements DatabaseHandlerAdminInterface {

    private static DatabaseHandlerAdminImp instance;

    /**
     * get new instance form this class with singleton
     *
     * @param void
     * @return instance of DatabaseHandlerAdminImp
     */
    public static synchronized DatabaseHandlerAdminImp getInstance() {
        if (instance == null) {
            instance = new DatabaseHandlerAdminImp() {
            };
        }
        return instance;
    }

    ////////////////////////////INSERT///////////////////////////////
    /**
     * add new item and it's specification if it is mobile
     *
     * @param item
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    @Override
    public void addItem(Item item) {
        Brand brand = item.getItemBrandId();
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.ADD_ITEM);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, item.getItemName());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(2, item.getItemQuantity());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(3, item.getItemPrice());
            DbOpenConnection.getInstance().getPreparedStatement().setString(4, item.getItemType());
            DbOpenConnection.getInstance().getPreparedStatement().setString(5, item.getAccessorieType());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(6, brand.getId());
            if (item.getItemReccId() != null) {
                DbOpenConnection.getInstance().getPreparedStatement().setInt(7, item.getItemReccId().getItemId());
                DbOpenConnection.getInstance().getPreparedStatement().setBlob(8, item.getImageStream());
                DbOpenConnection.getInstance().getPreparedStatement().setString(9, item.getItemDesc());
            } else {
                DbOpenConnection.getInstance().getPreparedStatement().setNull(7, java.sql.Types.INTEGER);
                DbOpenConnection.getInstance().getPreparedStatement().setBlob(8, item.getImageStream());
                DbOpenConnection.getInstance().getPreparedStatement().setString(9, item.getItemDesc());
            }
            int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();

            if (raw > 0) {
                System.out.println(raw + " item inserted");
                if (item.getItemType().equals("Mobile")) {
                    DbOpenConnection.getInstance().setPreparedStatement(Query.GET__MAX_ITEM);
                    ResultSet result = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
                    if (result.next()) {
                        addSpecifications(item, result.getInt(1));
                    }
                }
            } else {
                System.out.println("not inserted");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * add new specification for specific mobile
     *
     * @param itemId
     * @param item
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    @Override
    public void addSpecifications(Item item, int itemId) {
        Specifications sp = item.getSpecificationsList();
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.ADD_SPECIFICATION);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, itemId);
            DbOpenConnection.getInstance().getPreparedStatement().setString(2, sp.getNetworkTechnology());
            DbOpenConnection.getInstance().getPreparedStatement().setString(3, sp.getLaunchAnnounced());
            DbOpenConnection.getInstance().getPreparedStatement().setString(4, sp.getBodyDimensions());
            DbOpenConnection.getInstance().getPreparedStatement().setString(5, sp.getBodyWeight());
            DbOpenConnection.getInstance().getPreparedStatement().setString(6, sp.getBodyBuild());
            DbOpenConnection.getInstance().getPreparedStatement().setString(7, sp.getBodySim());
            DbOpenConnection.getInstance().getPreparedStatement().setString(8, sp.getDisplayType());
            DbOpenConnection.getInstance().getPreparedStatement().setString(9, sp.getDisplaySize());
            DbOpenConnection.getInstance().getPreparedStatement().setString(10, sp.getDisplayResolution());
            DbOpenConnection.getInstance().getPreparedStatement().setString(11, sp.getDisplayMultitouch());
            DbOpenConnection.getInstance().getPreparedStatement().setString(12, sp.getDisplayProtection());
            DbOpenConnection.getInstance().getPreparedStatement().setString(13, sp.getPlatformOs());
            DbOpenConnection.getInstance().getPreparedStatement().setString(14, sp.getPlatformChipset());
            DbOpenConnection.getInstance().getPreparedStatement().setString(15, sp.getPlatformCpu());
            DbOpenConnection.getInstance().getPreparedStatement().setString(16, sp.getPlatformGpu());
            DbOpenConnection.getInstance().getPreparedStatement().setString(17, sp.getMemoryCardslot());
            DbOpenConnection.getInstance().getPreparedStatement().setString(18, sp.getMemoryInternal());
            DbOpenConnection.getInstance().getPreparedStatement().setString(19, sp.getCameraPrimary());
            DbOpenConnection.getInstance().getPreparedStatement().setString(20, sp.getCameraFeatures());
            DbOpenConnection.getInstance().getPreparedStatement().setString(21, sp.getCameraVideo());
            DbOpenConnection.getInstance().getPreparedStatement().setString(22, sp.getCameraSecondary());
            DbOpenConnection.getInstance().getPreparedStatement().setString(23, sp.getSoundAlerttypes());
            DbOpenConnection.getInstance().getPreparedStatement().setString(24, sp.getSoundLoudspeaker());
            DbOpenConnection.getInstance().getPreparedStatement().setString(25, sp.getMmJack());
            DbOpenConnection.getInstance().getPreparedStatement().setString(26, sp.getCommsWlan());
            DbOpenConnection.getInstance().getPreparedStatement().setString(27, sp.getCommsBluetooth());
            DbOpenConnection.getInstance().getPreparedStatement().setString(28, sp.getCommsGps());
            DbOpenConnection.getInstance().getPreparedStatement().setString(29, sp.getCommsNfc());
            DbOpenConnection.getInstance().getPreparedStatement().setString(30, sp.getCommsRadio());
            DbOpenConnection.getInstance().getPreparedStatement().setString(31, sp.getCommsUsb());
            DbOpenConnection.getInstance().getPreparedStatement().setString(32, sp.getFeaturesSensors());
            DbOpenConnection.getInstance().getPreparedStatement().setString(33, sp.getFeaturesMessaging());
            DbOpenConnection.getInstance().getPreparedStatement().setString(34, sp.getFeaturesBrowser());
            DbOpenConnection.getInstance().getPreparedStatement().setString(35, sp.getFeaturesJava());
            DbOpenConnection.getInstance().getPreparedStatement().setString(36, sp.getBattery());
            DbOpenConnection.getInstance().getPreparedStatement().setString(37, sp.getBatteryStandby());
            DbOpenConnection.getInstance().getPreparedStatement().setString(38, sp.getBatteryTalktime());
            DbOpenConnection.getInstance().getPreparedStatement().setString(39, sp.getMiscColors());
            int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            if (raw > 0) {
                System.out.println(raw + " specification inserted");
            } else {
                System.out.println("specification not inserted");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * add new brand
     *
     * @param brand
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    @Override
    public void addBrand(Brand brand) {
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.ADD_BRAND);
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, brand.getBrandName());
            int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            if (raw > 0) {
                System.out.println(raw + " brand inserted");
            } else {
                System.out.println("not inserted");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    ////////////////////////////UPDATE///////////////////////////////
    /**
     **Update whole item with the specification and all band name if it is
     * mobile or accessories
     *
     * @param item
     * @param sp
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    @Override
    public void changeItem(Item item, Specifications sp) {
        Brand brand = item.getItemBrandId();
        try {
            if (item.getItemType().equals("Mobile")) {
                DbOpenConnection.getInstance().setPreparedStatement(Query.CHANGE_MOBILE);
                manipulateMobile(item, sp, brand);
                int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
                if (raw > 0) {
                    System.out.println(raw + "updated mobile");
                } else {
                    System.out.println("not updated");
                }
            } else {
                DbOpenConnection.getInstance().setPreparedStatement(Query.CHANGE_ACCESSORIE);
                manipulateAccesoiries(item, brand);
                int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
                if (raw > 0) {
                    System.out.println(raw + "updated accesoiries");
                } else {
                    System.out.println("not accesoiries updated");
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     **Update accessories
     *
     * @param item
     * @param brand
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    private void manipulateAccesoiries(Item item, Brand brand) {
//        Item recItem = item.getItemReccId();
        try {
            DbOpenConnection.getInstance().getPreparedStatement().setString(1, item.getItemName());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(2, item.getItemQuantity());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(3, item.getItemPrice());
            DbOpenConnection.getInstance().getPreparedStatement().setString(4, item.getItemType());
            DbOpenConnection.getInstance().getPreparedStatement().setString(5, item.getAccessorieType());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(6, brand.getId());
            if (item.getItemReccId() != null) {
                DbOpenConnection.getInstance().getPreparedStatement().setInt(7, item.getItemReccId().getItemId());
            } else {
                DbOpenConnection.getInstance().getPreparedStatement().setNull(7, java.sql.Types.INTEGER);
            }
            DbOpenConnection.getInstance().getPreparedStatement().setBlob(8, item.getImageStream());
            DbOpenConnection.getInstance().getPreparedStatement().setString(9, item.getItemDesc());
            DbOpenConnection.getInstance().getPreparedStatement().setInt(10, item.getItemId());

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     **Update Mobile
     *
     * @param item
     * @param brand
     * @param sp
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    private void manipulateMobile(Item item, Specifications sp, Brand brand) throws SQLException {
        DbOpenConnection.getInstance().getPreparedStatement().setString(1, item.getItemName());
        DbOpenConnection.getInstance().getPreparedStatement().setInt(2, item.getItemQuantity());
        DbOpenConnection.getInstance().getPreparedStatement().setInt(3, item.getItemPrice());
        DbOpenConnection.getInstance().getPreparedStatement().setString(4, item.getItemType());
        DbOpenConnection.getInstance().getPreparedStatement().setString(5, item.getAccessorieType());
        DbOpenConnection.getInstance().getPreparedStatement().setInt(6, brand.getId());
        if (item.getItemReccId() != null) {
            DbOpenConnection.getInstance().getPreparedStatement().setInt(7, item.getItemReccId().getItemId());
        } else {
            DbOpenConnection.getInstance().getPreparedStatement().setNull(7, java.sql.Types.INTEGER);
        }
        DbOpenConnection.getInstance().getPreparedStatement().setBlob(8, item.getImageStream());
        DbOpenConnection.getInstance().getPreparedStatement().setString(9, item.getItemDesc());
        DbOpenConnection.getInstance().getPreparedStatement().setString(10, sp.getNetworkTechnology());
        DbOpenConnection.getInstance().getPreparedStatement().setString(11, sp.getLaunchAnnounced());
        DbOpenConnection.getInstance().getPreparedStatement().setString(12, sp.getBodyDimensions());
        DbOpenConnection.getInstance().getPreparedStatement().setString(13, sp.getBodyWeight());
        DbOpenConnection.getInstance().getPreparedStatement().setString(14, sp.getBodyBuild());
        DbOpenConnection.getInstance().getPreparedStatement().setString(15, sp.getBodySim());
        DbOpenConnection.getInstance().getPreparedStatement().setString(16, sp.getDisplayType());
        DbOpenConnection.getInstance().getPreparedStatement().setString(17, sp.getDisplaySize());
        DbOpenConnection.getInstance().getPreparedStatement().setString(18, sp.getDisplayResolution());
        DbOpenConnection.getInstance().getPreparedStatement().setString(19, sp.getDisplayMultitouch());
        DbOpenConnection.getInstance().getPreparedStatement().setString(20, sp.getDisplayProtection());
        DbOpenConnection.getInstance().getPreparedStatement().setString(21, sp.getPlatformOs());
        DbOpenConnection.getInstance().getPreparedStatement().setString(22, sp.getPlatformChipset());
        DbOpenConnection.getInstance().getPreparedStatement().setString(23, sp.getPlatformCpu());
        DbOpenConnection.getInstance().getPreparedStatement().setString(24, sp.getPlatformGpu());
        DbOpenConnection.getInstance().getPreparedStatement().setString(25, sp.getMemoryCardslot());
        DbOpenConnection.getInstance().getPreparedStatement().setString(26, sp.getMemoryInternal());
        DbOpenConnection.getInstance().getPreparedStatement().setString(27, sp.getCameraPrimary());
        DbOpenConnection.getInstance().getPreparedStatement().setString(28, sp.getCameraFeatures());
        DbOpenConnection.getInstance().getPreparedStatement().setString(29, sp.getCameraVideo());
        DbOpenConnection.getInstance().getPreparedStatement().setString(30, sp.getCameraSecondary());
        DbOpenConnection.getInstance().getPreparedStatement().setString(31, sp.getSoundAlerttypes());
        DbOpenConnection.getInstance().getPreparedStatement().setString(32, sp.getSoundLoudspeaker());
        DbOpenConnection.getInstance().getPreparedStatement().setString(33, sp.getMmJack());
        DbOpenConnection.getInstance().getPreparedStatement().setString(34, sp.getCommsWlan());
        DbOpenConnection.getInstance().getPreparedStatement().setString(35, sp.getCommsBluetooth());
        DbOpenConnection.getInstance().getPreparedStatement().setString(36, sp.getCommsGps());
        DbOpenConnection.getInstance().getPreparedStatement().setString(37, sp.getCommsNfc());
        DbOpenConnection.getInstance().getPreparedStatement().setString(38, sp.getCommsRadio());
        DbOpenConnection.getInstance().getPreparedStatement().setString(39, sp.getCommsUsb());
        DbOpenConnection.getInstance().getPreparedStatement().setString(40, sp.getFeaturesSensors());
        DbOpenConnection.getInstance().getPreparedStatement().setString(41, sp.getFeaturesMessaging());
        DbOpenConnection.getInstance().getPreparedStatement().setString(42, sp.getFeaturesBrowser());
        DbOpenConnection.getInstance().getPreparedStatement().setString(43, sp.getFeaturesJava());
        DbOpenConnection.getInstance().getPreparedStatement().setString(44, sp.getBattery());
        DbOpenConnection.getInstance().getPreparedStatement().setString(45, sp.getBatteryStandby());
        DbOpenConnection.getInstance().getPreparedStatement().setString(46, sp.getBatteryTalktime());
        DbOpenConnection.getInstance().getPreparedStatement().setString(47, sp.getMiscColors());
        DbOpenConnection.getInstance().getPreparedStatement().setInt(48, item.getItemId());
        DbOpenConnection.getInstance().getPreparedStatement().setInt(49, item.getItemId());
    }

    ////////////////////////////DELETE///////////////////////////////
    /**
     * delete item and it's specification if it is mobile
     *
     * @param item
     * @return void
     * @throws SqlException if a sql statement update error occurs
     */
    @Override
    public void deleteItem(Item item) {
        try {
            if (item.getItemType().equals("Mobile")) {
                if (deleteSpecifications(item.getItemId())) {
                    System.out.println("delete sp");
                }
            }
            updateRecIdFK(item.getItemId());
            DbOpenConnection.getInstance().setPreparedStatement(Query.DELETE_ITEM);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, item.getItemId());
            int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            if (raw > 0) {
                System.out.println(raw + " deleted");
            } else {
                System.out.println("not Deletd");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * delete specific Specifications according to the item
     *
     * @param itemId
     * @return boolean if delete and false otherwise
     * @throws SqlException if a sql statement update error occurs
     */
    private boolean deleteSpecifications(int itemId) {
        Session session=DbOpenSession.getInstance().getSession();
        Specifications spec=new Specifications();
        session.delete(spec);
//        try {
//            DbOpenConnection.getInstance().setPreparedStatement(Query.DELETE_SPECIFICATION);
//            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, itemId);
//            int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
//            if (raw > 0) {
//                return true;
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return false;
    }

    /**
     * update all recommended item with specific the item
     *
     * @param itemId
     * @return boolean if delete and false otherwise
     * @throws SqlException if a sql statement update error occurs
     */
    private void updateRecIdFK(int itemId) {
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.UPDATE_REC_ID_FK);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, itemId);
            int raw = DbOpenConnection.getInstance().getPreparedStatement().executeUpdate();
            if (raw > 0) {
                System.out.println(raw + " updated");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<User> getUsers() {
        ArrayList<User> us = null;
        try {
            DbOpenConnection.getInstance().setPreparedStatement(Query.Get_Users);
            ResultSet rs = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            us = new ArrayList<>();
            while (rs.next()) {

                us.add(PopulateUser(rs));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }

        return us;
    }

    private User PopulateUser(ResultSet rs) {

        User u = new User();
        try {
            u.setId(rs.getInt(UserConstant.ID));
            u.setFirstName(rs.getString(UserConstant.FIRST_NAME));
            u.setLastName(rs.getString(UserConstant.LAST_NAME));
            u.setPassword(rs.getString(UserConstant.PASSWORD));
            u.setBalance(rs.getInt(UserConstant.BALANCE));
            u.setCity(rs.getString(UserConstant.CITY));
            u.setCountry(rs.getString(UserConstant.COUNTRY));
            u.setStreet(rs.getString(UserConstant.CITY));
            u.setEmail(rs.getString(UserConstant.EMAIL));
            u.setJob(rs.getString(UserConstant.JOB));
            //u.setCartShopList(cartShopList);
            //u.setHistoryList(historyList);

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }

    public ArrayList<CartShop> getCartShop(int uId) {
        ArrayList<CartShop> cartshops = new ArrayList<>();
        try {

            DbOpenConnection.getInstance().setPreparedStatement(Query.GET_ShopCart_By_UID);
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, uId);
            ResultSet rs = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            while (rs.next()) {
                System.out.println("there iss acart shpop");
                CartShop cs = new CartShop();
                cs.setPrice(rs.getInt(CartShopConstant.PRICE));
                cs.setQuantity(rs.getInt(CartShopConstant.QUANTITY));
                Item it = new Item();
                it.setItemName(rs.getString(ItemConstant.ITEM_NAME));
                cs.setItem(it);
                cartshops.add(cs);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cartshops;
    }

    public String getUserByID(int uId) {
        String res = null;
        try {
            DbOpenConnection.getInstance().setPreparedStatement("select FirstName,LastName from users where id=?");
            DbOpenConnection.getInstance().getPreparedStatement().setInt(1, uId);
            ResultSet rs = DbOpenConnection.getInstance().getPreparedStatement().executeQuery();
            if (rs.next()) {
                res = rs.getString(UserConstant.FIRST_NAME) + " " + rs.getString(UserConstant.LAST_NAME);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandlerAdminImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

}
