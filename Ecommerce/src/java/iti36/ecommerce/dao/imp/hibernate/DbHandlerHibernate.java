/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.imp.hibernate;

import iti36.ecommerce.dao.util.DbOpenSession;
import iti36.ecommerce.mapping.CartShop;
import iti36.ecommerce.mapping.History;
import iti36.ecommerce.mapping.Item;
import iti36.ecommerce.mapping.Users;
import iti36.ecommerce.util.ImageUtil;
import iti36.ecommerce.util.TransactionError;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import iti36.ecommerce.dao.api.hibernate.DbHandlerHibernateInterface;
import iti36.ecommerce.mapping.Brand;
import iti36.ecommerce.mapping.CartShopId;
import iti36.ecommerce.mapping.Credit;
import iti36.ecommerce.mapping.Specifications;
import java.util.Vector;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;

/**
 *
 * @author adel
 */
public class DbHandlerHibernate implements DbHandlerHibernateInterface {

    @Override
    public Users logIn(Users user) {
        Session session = DbOpenSession.getInstance().getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Users u where u.email = :email and u.password = :password").
                setString("email", user.getEmail()).setString("password", user.getPassword()).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        user = (Users) query.uniqueResult();
        return getHistorAndCartShop(user);
    }

    @Override
    public Users signUp(Users user) {
        Session session = DbOpenSession.getInstance().getSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        return user;
    }

    @Override
    public Users checkOut(Users user, int totalPrice) {
        Session session = DbOpenSession.getInstance().getSession();
        session.beginTransaction();
        Set<CartShop> cartShopSet = (Set<CartShop>) user.getCartShops();
        Iterator<CartShop> cartShopIterator = cartShopSet.iterator();
        while (cartShopIterator.hasNext()) {
            CartShop cartShop = cartShopIterator.next();
            Criteria criteria = session.createCriteria(Item.class)
                    .add(Restrictions.eq("id", cartShop.getItem().getItemId()));
            Item item = (Item) criteria.uniqueResult();
            if (item.getItemQuantity() < cartShop.getQuantity()) {
                TransactionError.setItemError(item.getItemId());
                return user;
            } else {
                // change ITem Quantity
                item.setItemQuantity(item.getItemQuantity() - cartShop.getQuantity());
                session.saveOrUpdate(item);
                // add item in history
                History history = new History();
                history.setDate(new Date());
                history.setItem(item);
                history.setPrice(item.getItemPrice());
                history.setQuantity(cartShop.getQuantity());
                history.setUsers(user);
                session.saveOrUpdate(history);
                // remove item from cartShop
                session.delete(cartShop);
            }
        }
        user.setBalance(user.getBalance() - totalPrice);
        session.saveOrUpdate(user);
        session.getTransaction().commit();
        session.refresh(user);
        return user;
    }

    @Override
    public List<Users> getUsers() {
        Criteria criteria = DbOpenSession.getInstance().getSession().createCriteria(Users.class);
        return criteria.list();

    }

    @Override
    public List<History> getUserHistoryByUserId(Users user) {
        Criteria criteria = DbOpenSession.getInstance().getSession().createCriteria(History.class)
                .add(Restrictions.eq("users", user));
        return criteria.list();
    }

    @Override
    public List<CartShop> getUserCartShopByUserId(Users user) {
        Criteria criteria = DbOpenSession.getInstance().getSession().createCriteria(CartShop.class)
                .add(Restrictions.eq("users", user));
        return criteria.list();
    }

    @Override
    public Users cartOperation(Users user, int itemId, String type) {
        if (user != null && user.getCartShops() != null) {
            Session session = DbOpenSession.getInstance().getSession();
            session.beginTransaction();
            Set<CartShop> cartShopSet = (Set<CartShop>) user.getCartShops();
            Iterator<CartShop> cartShopIterator = cartShopSet.iterator();
            CartShop cartShop = null;
            while (cartShopIterator.hasNext()) {
                cartShop = cartShopIterator.next();
                if (cartShop.getItem().getItemId() == itemId) {
                    break;
                } else {
                    cartShop = null;
                }
            }
            if (cartShop != null && type.equals("addToCart")) {
                type = "plus";
            } else if (cartShop == null && type.equals("addToCart")) {
                return AddCartItem(user, String.valueOf(itemId));
            }
            if (cartShop != null && type.equals("plus")) {
                cartShop.setQuantity(cartShop.getQuantity() + 1);
            } else if (cartShop != null && type.equals("sub") && cartShop.getQuantity() > 1) {
                cartShop.setQuantity(cartShop.getQuantity() - 1);
            } else if (cartShop != null && type.equals("delete")) {
                cartShopSet.remove(cartShop);
            }
            session.saveOrUpdate(user);
            session.getTransaction().commit();
            session.refresh(user);
        }
        return user;
    }

    @Override
    public Users AddCartItem(Users user, String itemid) {
        Session session = DbOpenSession.getInstance().getSession();
        CartShop cart = new CartShop();
        Criteria criteria = session.createCriteria(Item.class)
                .add(Restrictions.eq("id", Integer.parseInt(itemid)));
        Item item = (Item) criteria.uniqueResult();
        CartShopId id = new CartShopId();
        id.setUserId(user.getId());
        id.setItemId(Integer.parseInt(itemid));
        cart.setId(id);
        cart.setQuantity(1);
        cart.setPrice(item.getItemPrice());
        cart.setUsers(user);
        cart.setItem(item);
        session.beginTransaction();
        session.save(cart);
        session.getTransaction().commit();
        session.refresh(user);
        user = getHistorAndCartShop(user);
        return user;
    }

    private Users getHistorAndCartShop(Users user) {
        if (user != null && user.getHistories() != null) {
            Set<History> historySet = (Set<History>) user.getHistories();
            Iterator<History> historyIterator = historySet.iterator();
            while (historyIterator.hasNext()) {
                History history = historyIterator.next();
                history.getItem().setImageBase64Binary(ImageUtil.resizeImage(history.getItem().getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));

            }
        }
        if (user != null && user.getCartShops() != null) {
            Set<CartShop> cartShopSet = (Set<CartShop>) user.getCartShops();
            Iterator<CartShop> cartShopIterator = cartShopSet.iterator();
            while (cartShopIterator.hasNext()) {
                CartShop cartShop = cartShopIterator.next();
                cartShop.getItem().setImageBase64Binary(ImageUtil.resizeImage(cartShop.getItem().getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
            }
        }
        return user;
    }

    @Override
    public Vector<Brand> getAllItemBrand() {
        Vector<Brand> brands = new Vector<>();
        try {
            Session session = DbOpenSession.getInstance().getSession();
            session.beginTransaction();
            Query hqlQuery = session.createQuery("from Brand");
            List<Brand> result = hqlQuery.list();
            Brand brand;
            for (Brand b : result) {
                System.out.println(b.getBrandName());
                System.out.println(b.getId());
                brand = new Brand();
                brand.setId(b.getId());
                brand.setBrandName(b.getBrandName());
                brands.add(brand);
            }

            return brands;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return brands;
    }

    @Override
    public Vector<String> getAllAccessorieType() {
        Vector<String> accessorie = new Vector<>();
        try {
            Session session = DbOpenSession.getInstance().getSession();
            Query hqlQuery2 = session.createQuery("SELECT DISTINCT i.accessorieType FROM Item i WHERE i.itemType = 'accessorie'");
            List<String> result2 = hqlQuery2.list();
            for (String b : result2) {
                System.out.println(b);
                accessorie.add(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accessorie;
    }

    @Override
    public Vector<Item> findItemsByExample(Item item, int low, int high) {
        Session session = DbOpenSession.getInstance().getSession();
        Vector<Item> items = new Vector<>();
        Example exampleItem = Example.create(item).ignoreCase().excludeProperty("itemPrice").excludeProperty("itemQuantity");
        Brand brand = item.getBrand();
        Criteria itemSearch;
        if (brand.getId() != null) {
            System.out.println("session >>" + session == null);
            itemSearch = session.createCriteria(Item.class, "i")
                    .add(exampleItem).add(Restrictions.between("i.itemPrice", low, high))
                    .add(Restrictions.eq("brand", brand));
        } else {
            itemSearch = session.createCriteria(Item.class, "i")
                    .add(exampleItem).add(Restrictions.between("i.itemPrice", low, high));

        }
        List it1 = itemSearch.list();
        Iterator it = it1.iterator();
        while (it.hasNext()) {
            Item item1 = (Item) it.next();
            item1.setImageBase64Binary(ImageUtil.resizeImage(item1.getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
            items.add(item1);
            System.out.println("item>> " + item1.getItemName());
        }
        return items;
    }

    @Override
    public List<Item> getAllItems(int from, int to) {
        Criteria criteria = DbOpenSession.getInstance().getSession().createCriteria(Item.class);
        criteria.setFirstResult(from);
        criteria.setMaxResults(to);
        List<Item> Items = criteria.list();
        for (Item item : Items) {
            item.setImageBase64Binary(ImageUtil.resizeImage(item.getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        }
        return Items;
    }

    @Override
    public boolean checkcardNumber(int cardNumber) {
        return (boolean) DbOpenSession.getInstance().getSession().createCriteria(Credit.class)
                .add(Restrictions.eq("creditNumber", cardNumber))
                .setProjection(Projections.property("creditValid"))
                .uniqueResult();
    }

    /**
     * update user balance
     *
     * @param cardNumber
     * @param user
     * @return user after update his new balance
     */
    @Override
    public Users updateUserBalance(int cardNumber, Users user) {
        Session session = DbOpenSession.getInstance().getSession();
        int balance = (int) session.createCriteria(Credit.class)
                .add(Restrictions.eq("creditNumber", cardNumber))
                .setProjection(Projections.property("creditBalance"))
                .uniqueResult();
        int newBalance = balance + user.getBalance();
        user.setBalance(newBalance);
        invalidateCard(cardNumber);
        session.beginTransaction();
        session.createQuery("UPDATE Users user SET user.balance = :newBalance WHERE user.id = :userId")
                .setInteger("newBalance", newBalance)
                .setInteger("userId", user.getId())
                .executeUpdate();
        session.getTransaction().commit();
        return user;
    }

    /**
     * update credit to be invalid
     *
     * @param cardNumber
     */
    private void invalidateCard(int cardNumber) {
        Session session = DbOpenSession.getInstance().getSession();
        session.beginTransaction();
        session.createQuery("UPDATE Credit credit SET credit.creditValid = :notValid WHERE credit.creditNumber = :cardNumber")
                .setInteger("notValid", 0)
                .setInteger("cardNumber", cardNumber)
                .executeUpdate();
        session.getTransaction().commit();
    }
      @Override
    public Vector<Item> getHomeItems() {
        Vector<Item> items = new Vector<>();
        Session session = DbOpenSession.getInstance().getSession();
        String QuaryString = "from Item as I ";
        session.beginTransaction();
        List newList = session.createQuery(QuaryString).setMaxResults(12).list();
        Iterator pairs = newList.iterator();
        while (pairs.hasNext()) {
            Item item = (Item) pairs.next();
            item.setImageBase64Binary(ImageUtil.resizeImage(item.getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
            items.add(item);
            System.err.println("item " + item.getItemName() + "is added to vector");
        }
        return items;
    }

    @Override
    public Item getItemById(int pId) {
        Session session = DbOpenSession.getInstance().getSession();
        String QuaryString = "from Item as I where I.itemId = :no ";
        session.beginTransaction();
        Item item = (Item) session.createQuery(QuaryString).setInteger("no", pId).setMaxResults(1).uniqueResult();
        item.setImageBase64Binary(ImageUtil.resizeImage(item.getItemImage(), ImageUtil.WIDTH, ImageUtil.HIGHT));
        System.err.println("item  name " + item.getItemType());
        return item;
    }

    @Override
    public Specifications getSpecByItemId(int pId) {
        Session session = DbOpenSession.getInstance().getSession();
        String QuaryString = "select specifications from Item as I where I.itemId = :no ";
        session.beginTransaction();
        Specifications sp = (Specifications) session.createQuery(QuaryString).setInteger("no", pId).setMaxResults(1).uniqueResult();
        return sp;
    }
        
    /**
     * get number of items in shop
     *
     * @return String contains size of items
     */
    @Override
    public String getItemLength() {
        return DbOpenSession.getInstance().getSession().createCriteria(Item.class)
                .setProjection(Projections.rowCount()).uniqueResult().toString();
    }

    /**
     * add new item and it's specification if it is mobile
     *
     * @param item
     */
    @Override
    public void addItem(Item item) {
        Session session = DbOpenSession.getInstance().getSession();
        session.beginTransaction();
        session.save(item);
        if(item.getItemType().equals("Mobile"))
            session.save(item.getSpecifications());
        session.getTransaction().commit();
    }
    
    
    public void deleteItem(Item itemtobedeleted) {
        Session session = DbOpenSession.getInstance().getSession();
        session.clear();
        if (itemtobedeleted.getItemType().equals("Mobile")) {
          
            Specifications spec = new Specifications();
            spec.setSpecItemId(itemtobedeleted.getItemId());
            session.beginTransaction();
            session.delete(spec);
            session.getTransaction().commit();
        }
        itemtobedeleted = (Item) session.get(Item.class, itemtobedeleted.getItemId());
       if(itemtobedeleted.getItems()!=null)
       { Iterator it = itemtobedeleted.getItems().iterator();
        while (it.hasNext()) {
            Item item_rec = (Item) it.next();
            item_rec.setItem(null);
            session.beginTransaction();
            session.saveOrUpdate(item_rec);
            session.getTransaction().commit();
            System.out.println("updating");
        }
       }
        session.beginTransaction();
        session.delete(itemtobedeleted);
        session.getTransaction().commit();
        System.out.println("deleted");
        // session.close();
    }
}
