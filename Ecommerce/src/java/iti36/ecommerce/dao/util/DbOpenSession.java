/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.util;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author adel
 */
public class DbOpenSession {

    private SessionFactory sessionfactory;
    private Session session;
    private static DbOpenSession instance;

    public static synchronized DbOpenSession getInstance() {
        if (instance == null) {
            instance = new DbOpenSession();
        }
        return instance;
    }

    private DbOpenSession() {
        openconnection();
    }

    private void openconnection() {
        try {
            sessionfactory = new Configuration().configure().buildSessionFactory();
            session = sessionfactory.openSession();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void closeConnection() {
        try {

        } catch (Exception ex) {
            Logger.getLogger(DbOpenSession.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public SessionFactory getSessionfactory() {
        return sessionfactory;
    }

    public void setSessionfactory(SessionFactory sessionfactory) {
        this.sessionfactory = sessionfactory;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
