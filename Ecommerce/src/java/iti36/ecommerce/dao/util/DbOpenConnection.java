/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.dao.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author adel
 */
public class DbOpenConnection {

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private static DbOpenConnection instance;

    public static synchronized DbOpenConnection getInstance() {
        if (instance == null) {
            instance = new DbOpenConnection();
        }
        return instance;
    }

    private DbOpenConnection() {
        openconnection();
    }

    private void openconnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/ecommercedb?zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=UTF-8","root","");
            statement = connection.createStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbOpenConnection.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }
      public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public void setPreparedStatement(String sql) throws SQLException {
        preparedStatement = connection.prepareStatement(sql);
    }

    public void setPreparedStatement(String sql,String []key) throws SQLException {
        preparedStatement = connection.prepareStatement(sql,key);
    }
 
}
