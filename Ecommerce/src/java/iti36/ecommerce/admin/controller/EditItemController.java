/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.admin.controller;

import iti36.ecommerce.dao.util.DbOpenSession;
//import iti36.ecommerce.entity.Brand;
//import iti36.ecommerce.entity.Item;
//import iti36.ecommerce.entity.Specifications;
import iti36.ecommerce.mapping.Item;
import iti36.ecommerce.mapping.Brand;
import iti36.ecommerce.mapping.Specifications;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Session;
/**
 *
 * @author user
 */
@MultipartConfig(maxFileSize = 16177215) 
public class EditItemController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditItemController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditItemController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Session session = DbOpenSession.getInstance().getSession();
            session.beginTransaction();
            Item item = (Item) session.load(Item.class, new Integer(request.getParameter("ItemID")));
            Specifications sp = item.getSpecifications();
            sp.setNetworkTechnology((String) request.getParameter("networkTechnology"));
            sp.setLaunchAnnounced((String) request.getParameter("launchAnnounced"));
            sp.setBodyDimensions((String) request.getParameter("bodyDimensions"));
            sp.setBodyWeight((String) request.getParameter("bodyWeight"));
            sp.setBodyBuild((String) request.getParameter("bodyBuild"));
            sp.setBodySim((String) request.getParameter("bodySim"));
            sp.setDisplayType((String) request.getParameter("displayType"));
            sp.setDisplayResolution((String) request.getParameter("displayResolution"));
            sp.setDisplayProtection((String) request.getParameter("displayProtection"));
            sp.setPlatformOs((String) request.getParameter("platformOs"));
            sp.setPlatformChipset((String) request.getParameter("platformChipset"));
            sp.setPlatformCpu((String) request.getParameter("platformCpu"));
//            sp.setMemoryCardslot((String) request.getParameter("memoryCardslot"));
//            sp.setMemoryInternal((String) request.getParameter("memoryInternal"));
            sp.setMemoryCardSlot((String) request.getParameter("memoryCardslot"));
            sp.setMemoryInternal((String) request.getParameter("memoryInternal"));
            sp.setCameraPrimary((String) request.getParameter("cameraPrimary"));
//            Item item = new Item();
            item.setItemId(Integer.parseInt(request.getParameter("ItemID")));
            item.setItemName(request.getParameter("ItemName"));
            item.setItemPrice(Integer.parseInt(request.getParameter("itemPrice")));
            item.setItemQuantity(Integer.parseInt(request.getParameter("itemQuantity")));
            item.setItemType(request.getParameter("itemType"));
            item.setAccessorieType(request.getParameter("accessoryType"));
            item.setItemDesc(request.getParameter("itemDesc"));

            Brand brand = new Brand();
            String brandname = (request.getParameter("BrandId"));
            brand.setBrandName(request.getParameter("BrandId"));
            ServletContext context = request.getSession().getServletContext();
            Vector<Brand> brands = (Vector<Brand>) context.getAttribute("brand");
            for (int i = 0; i < brands.size(); i++) {
                if (brands.get(i).getBrandName().equals(brandname)) {
                    brand.setId(brands.get(i).getId());
                }
            }
//          item.setItemBrandId(brand);
            item.setBrand(brand);
            item.setSpecifications(sp);
            sp.setItem(item);
            InputStream inputStream = null;
            Part filePart = request.getPart("imageBase64Binary");
            if (filePart != null) {
                inputStream = filePart.getInputStream();
//                item.setImageStream(inputStream);
                byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                item.setItemImage(buffer);
            }
//            DatabaseHandlerAdminImp.getInstance().changeItem(item, sp);
//            new DbHandlerHibernate().updateItem(item);
            session.getTransaction().commit();
            response.sendRedirect("/Ecommerce/admin");
        } catch (Exception ex) {
            Logger.getLogger(EditItemController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
