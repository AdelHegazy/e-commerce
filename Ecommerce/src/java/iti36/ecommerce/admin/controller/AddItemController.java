/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.admin.controller;
import iti36.ecommerce.dao.imp.hibernate.DbHandlerHibernate;
import iti36.ecommerce.mapping.Item;
import iti36.ecommerce.mapping.Brand;
import iti36.ecommerce.mapping.Specifications;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Shika
 */
@MultipartConfig(maxFileSize = 16177215)
public class AddItemController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddItemControlle</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddItemControlle at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/admin.jsp/AddItem.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Item item = new Item();
        Brand brand = new Brand();
        Specifications sp = new Specifications();
        if (request.getParameter("itemType").equals("Mobile")) {
            sp.setNetworkTechnology((String) request.getParameter("networkTechnology"));
            sp.setLaunchAnnounced((String) request.getParameter("announced"));
            sp.setPlatformOs((String) request.getParameter("os"));
            sp.setDisplaySize((String) request.getParameter("screenSize"));
            sp.setDisplayType((String) request.getParameter("screenType"));
            sp.setFeaturesSensors((String) request.getParameter("Sensors"));
            sp.setSoundLoudspeaker((String) request.getParameter("loudspeaker"));
            sp.setSoundJack((String) request.getParameter("jack"));
            sp.setMemoryCardSlot((String) request.getParameter("CardSlot"));
            sp.setMemoryInternal((String) request.getParameter("internal"));
            sp.setCommsWlan((String) request.getParameter("wifi"));
            sp.setCommsGps((String) request.getParameter("GPS"));
            sp.setCommsBluetooth((String) request.getParameter("bluetooth"));
            sp.setCameraPrimary((String) request.getParameter("primary"));
            sp.setCameraSecondary((String) request.getParameter("secondary"));
            sp.setPlatformCpu((String) request.getParameter("CPU"));
            sp.setPlatformGpu((String) request.getParameter("GPU"));
//            item.setSpecificationsList(sp);
            item.setSpecifications(sp);
        }
        item.setItemName(request.getParameter("itemName"));
        item.setItemQuantity(Integer.parseInt(request.getParameter("itemQuantity")));
        item.setItemPrice(Integer.parseInt(request.getParameter("itemPrice")));
        item.setItemType(request.getParameter("itemType"));
        item.setAccessorieType(request.getParameter("accessoryType"));
        String brandname = (request.getParameter("brandName"));
        brand.setBrandName(request.getParameter("brandName"));
        ServletContext context = request.getSession().getServletContext();
        Vector<Brand> brands = (Vector<Brand>) context.getAttribute("brand");
        for (int i = 0; i < brands.size(); i++) {
            if (brands.get(i).getBrandName().equals(brandname)) {
                brand.setId(brands.get(i).getId());
            }
        }
//        item.setItemBrandId(brand);
        item.setBrand(brand);
        InputStream inputStream = null;
        Part filePart = request.getPart("image");
        if (filePart != null) {
            inputStream = filePart.getInputStream();
//            item.setImageStream(inputStream);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            item.setItemImage(buffer);
        }
        item.setItemDesc(request.getParameter("descrition"));
//        DatabaseHandlerAdminImp.getInstance().addItem(item);
        sp.setItem(item);
        new DbHandlerHibernate().addItem(item);
        response.sendRedirect("/Ecommerce/admin");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
