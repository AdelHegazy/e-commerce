/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.constatnt;

/**
 
 * @author adel
 */
public class Query {

    public static final String GET_PIC = "SELECT `item-image` FROM `item` WHERE `item-id`= ?";
    public static final String CART_ITEM_COUNT = "SELECT COUNT(*) FROM `cart-shop`";
    public static final String CART_USERS_ITEMS = "SELECT * FROM `cart-shop` WHERE `user-id`= ?";
    public static final String ONE_CART_ITEM = "SELECT * FROM `item` WHERE `item-id`= ?";
    public static final String CHNG_CART_ITEM_QUANTITY = "UPDATE `cart-shop` SET `quantity`= ? WHERE `user-id`= ? and `item-id` = ?";
    public static final String DELETE_CART_ITEM = "DELETE FROM `cart-shop` WHERE `item-id`= ? AND `user-id` = ?";
    public static final String GET_HOME_ITEMS = "SELECT * FROM `item` ORDER BY `item-id` DESC LIMIT 15";
    public static final String GET_ALL_ITEMS = "SELECT * FROM `item`";
    public static final String GET_ALL_BRAND = "SELECT * FROM `brand`";
    public static final String GET_ALL_Accessorie_TYPE = "SELECT DISTINCT `accessorie-type` FROM `item` WHERE `item-type` = \"accessorie\"";
//    public static final String CHANGE_ITEM_PRICE = "UPDATE `item` SET `item-price` = ? WHERE `item-id` = ?";
    public static final String CHANGE_ITEM_QUANTITY = "UPDATE `item` SET `item-quantity` = ? WHERE `item-id` = ?";
//    public static final String ADD_ITEM = "INSERT INTO `ecommercedb`.`item` (`item-name`, `item-quantity`, `item-price`, `item-type`, `accessorie-type`, `item-brand-id`, `item-recc-id`, `item-image`, `item-desc`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
//    public static final String DELETE_ITEM = "DELETE FROM `item` WHERE `item-id` = ?";
    public static final String ADD_CART_ITEM = "INSERT INTO `cart-shop`(`user-id`, `item-id`, `quantity`, `price`) VALUES (?,?,?,?)";
    public static final String COUNT = "SELECT COUNT(*) FROM  users where email = ? and password = ?";
    public static final String LOG_IN = "select * from `users` where `Email` = ? and `password` = ?";
    public static final String GET_ONE_ITEM = "SELECT  `item-quantity` , `item-price` FROM `item` WHERE `item-id` = ?";
    public static final String UPDATE_USER_BALANCE = "UPDATE `users` SET `balance` = ? WHERE `id` = ?";
    public static final String ADD_ITEM_HISTORY = "INSERT INTO `history`(`user-id`, `item-id`, `quantity`, `price`, `date`) VALUES (?,?,?,?,?)";
    public static final String DELETE_CART_SHOP_TO_USER = "DELETE FROM `cart-shop` WHERE `item-id`=? and `user-id`=?";
    public static final String GET_USER_ITEM_HISTORY = "SELECT * FROM `history` WHERE `user-id`=?";     
    public static final String CHANGE_MOBILE = "UPDATE `item`,`specifications` SET \n" +
"`item-name`= ?,\n" +
"`item-quantity`=?,\n" +
"`item-price`=?,\n" +
"`item-type`=?,\n" +
"`accessorie-type`=?,\n" +
"`item-brand-id`=?,\n" +
"`item-recc-id`=?,\n" +
"`item-image`=?,\n" +
"`item-desc`=? ,\n" +
"`NETWORK-Technology`=?,\n" +
"`LAUNCH-Announced`=?,\n" +
"`BODY-Dimensions`=?,\n" +
"`BODY-Weight`=?,\n" +
"`BODY-Build`=?,\n" +
"`BODY-SIM`=?,\n" +
"`DISPLAY-Type`=?,\n" +
"`DISPLAY-Size`=?,\n" +
"`DISPLAY-Resolution`=?,\n" +
"`DISPLAY-Multitouch`=?,\n" +
"`DISPLAY-Protection`=?,\n" +
"`PLATFORM-OS`=?,\n" +
"`PLATFORM-Chipset`=?,\n" +
"`PLATFORM-CPU`=?,\n" +
"`PLATFORM-GPU`=?,\n" +
"`MEMORY-Card slot`=?,\n" +
"`MEMORY-Internal`=?,\n" +
"`CAMERA-Primary`=?,\n" +
"`CAMERA-Features`=?,\n" +
"`CAMERA-Video`=?,\n" +
"`CAMERA-Secondary`=?,\n" +
"`SOUND-Alert types`=?,\n" +
"`SOUND-Loudspeaker`=?,\n" +
"`SOUND-3.5mm jack`=?,\n" +
"`COMMS-WLAN`=?,\n" +
"`COMMS-Bluetooth`=?,\n" +
"`COMMS-GPS`=?,\n" +
"`COMMS-NFC`=?,\n" +
"`COMMS-Radio`=?,\n" +
"`COMMS-USB`=?,\n" +
"`FEATURES-Sensors`=?,\n" +
"`FEATURES-Messaging`=?,\n" +
"`FEATURES-Browser`=?,\n" +
"`FEATURES-Java`=?,\n" +
"`BATTERY`=?,\n" +
"`BATTERY-Stand-by`=?,\n" +
"`BATTERY-Talk time`=?,\n" +
"`MISC-Colors`=? WHERE `spec-item-id` = ? AND `item-id` = ?";

    public static final String CHANGE_ACCESSORIE = "UPDATE `item` SET `item-name`=?,`item-quantity`=?,`item-price`=?,`item-type`=?,`accessorie-type`=?,`item-brand-id`=?,`item-recc-id`=?,`item-image`=?,`item-desc`=? WHERE `item-id` = ?";
    public static final String DELETE_ITEM = "DELETE FROM `item` WHERE `item-id` = ?";
    public static final String DELETE_SPECIFICATION = "DELETE FROM `specifications` WHERE `spec-item-id` = ?";
    public static final String UPDATE_REC_ID_FK = "UPDATE `item` SET `item-recc-id`= NULL WHERE `item-recc-id` = ?";
    public static final String ADD_BRAND = "INSERT INTO `brand` (`brand-name`) VALUES (?)";
    public static final String ADD_SPECIFICATION = "INSERT INTO `specifications` (`spec-item-id`, `NETWORK-Technology`, `LAUNCH-Announced`, `BODY-Dimensions`, `BODY-Weight`, `BODY-Build`, `BODY-SIM`, `DISPLAY-Type`, `DISPLAY-Size`, `DISPLAY-Resolution`, `DISPLAY-Multitouch`, `DISPLAY-Protection`, `PLATFORM-OS`, `PLATFORM-Chipset`, `PLATFORM-CPU`, `PLATFORM-GPU`, `MEMORY-Card slot`, `MEMORY-Internal`, `CAMERA-Primary`, `CAMERA-Features`, `CAMERA-Video`, `CAMERA-Secondary`, `SOUND-Alert types`, `SOUND-Loudspeaker`, `SOUND-3.5mm jack`, `COMMS-WLAN`, `COMMS-Bluetooth`, `COMMS-GPS`, `COMMS-NFC`, `COMMS-Radio`, `COMMS-USB`, `FEATURES-Sensors`, `FEATURES-Messaging`, `FEATURES-Browser`, `FEATURES-Java`, `BATTERY`, `BATTERY-Stand-by`, `BATTERY-Talk time`, `MISC-Colors`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String ADD_ITEM = "INSERT INTO `item` (`item-name`, `item-quantity`, `item-price`, `item-type`, `accessorie-type`, `item-brand-id`, `item-recc-id`, `item-image`, `item-desc`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String GET__MAX_ITEM = "SELECT MAX(`item-id`) FROM `item`";
    public static final String getBrandName="SELECT * FROM `brand` , `item` where item.`item-brand-id`=brand.id AND item.`item-id`=?";
    public static final String CARD = "SELECT * FROM  `credit` where `credit-number` = ?";
    public static final String INVALIDATE_CARD = "UPDATE `credit` SET `credit-valid`= 0 WHERE `credit-number`= ?";
    public static final String Get_Users="select * from users ";
    public static final String GET_ShopCart_By_UID="SELECT `cart-shop`.`quantity`,`cart-shop`.`price`,item.`item-name` FROM `cart-shop`,item WHERE `cart-shop`.`item-id`=item.`item-id` AND `cart-shop`.`user-id`=?";
 public static final String GET_History_By_UID="SELECT item.`item-name`, `quantity`, `price`, `date` FROM `history` ,item WHERE item.`item-id`=history.`item-id` AND`user-id`=?";
}
