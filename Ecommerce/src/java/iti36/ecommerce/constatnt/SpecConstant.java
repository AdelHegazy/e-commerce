/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.constatnt;

/**
 *
 * @author yoka
 */
public class SpecConstant {

    public static final String spec_id = "ID";
    public static final String item_id = "spec-item-id";
    public static final String net_tech = "NETWORK-Technology";
    public static final String launched = "LAUNCH-Announced";
    public static final String body_dim = "BODY-Dimensions";
    public static final String body_weight = "BODY-Weight";
    public static final String body_build = "BODY-Build";
    public static final String body_sim = "BODY-SIM";
    public static final String display_type = "DISPLAY-Type";
    public static final String display_size = "DISPLAY-Size";
    public static final String display_res = "DISPLAY-Resolution";
    public static final String display_touch = "DISPLAY-Multitouch";
    public static final String display_protect = "DISPLAY-Protection";
    public static final String os = "PLATFORM-OS";
    public static final String chipset = "PLATFORM-Chipset";
    public static final String cpu = "PLATFORM-CPU";
    public static final String gpu = "PLATFORM-GPU";
    public static final String card_slot = "MEMORY-Card slot";
    public static final String mem_internal = "MEMORY-Internal";
    public static final String camera_primary = "CAMERA-Primary";
    public static final String camera_features = "CAMERA-Features";
    public static final String camera_video = "CAMERA-Video";
    public static final String camera_sec = "CAMERA-Secondary";
    public static final String alert = "SOUND-Alert types";
    public static final String loudspeaker = "SOUND-Loudspeaker";
    public static final String mm_jack = "SOUND-3.5mm jack";
    public static final String wlan = "COMMS-WLAN";
    public static final String bluetooth = "COMMS-Bluetooth";
    public static final String gps = "COMMS-GPS";
    public static final String nfs = "COMMS-NFC";
    public static final String radio = "COMMS-Radio";
    public static final String usb = "COMMS-USB";
    public static final String sensor="FEATURES-Sensors";
    public static final String msg="FEATURES-Messaging";
    public static final String browser="FEATURES-Browser";
    public static final String java="FEATURES-Java";
    public static final String battery="BATTERY";
    public static final String battery_standby="BATTERY-Stand-by";
    public static final String battery_talktime="BATTERY-Talk time";
    public static final String misc="MISC-Colors";
                                                    
                                                    
                            
                
}
