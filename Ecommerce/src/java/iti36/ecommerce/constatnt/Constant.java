/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.constatnt;

/**
 *
 * @author Shika
 */
public class Constant {

    public static final String GET_ALL_ITEMS = "SELECT * FROM `item`";
    public static final String ITEM_ID = "item-id";
    public static final String SingUpQuary = "insert into users (FirstName,LastName,Email,password,job,country,city,street,balance)values(?,?,?,?,?,?,?,?,?) ";
    public static final String searchwithBrandID = " select item.`item-id` , item.`item-name` , item.`item-price` , item.`item-type`,item.`item-desc`,`item-image`,`item-quantity` from item where item.`item-brand-id`= ? ";
    public static final String searchwithBrandIDandtype = "select item.`item-id` , item.`item-name` , item.`item-price` , item.`item-type`,item.`item-desc`,`item-image`,`item-quantity` from item where item.`item-brand-id`= ? and item.`item-type` like ?";
    public static final String searchwithBrandIDandtypeAndPrice = "select item.`item-id` , item.`item-name` , item.`item-price` , item.`item-type`,item.`item-desc`,`item-image`,`item-quantity` from item where item.`item-brand-id`= ? and item.`item-type` like ? and item.`item-price` BETWEEN ? AND ? ";
    public static final String searchWithItemTypeAndPrice = "select item.`item-id` , item.`item-name` , item.`item-price` , item.`item-type`,item.`item-desc`,`item-image`,`item-quantity` from item where item.`item-type` = ? AND item.`item-price` BETWEEN ? AND ?";
    public static final String GET_ALL_ACCESSORIE = "select * from item where `accessorie-type` = ? and `item-price` BETWEEN ? AND ? ";
    public static final String GET_ITEM_BY_ID=" select item.`item-id` , item.`item-name` , item.`item-price` , item.`item-type`,item.`item-desc`,`item-image`,`item-quantity` ,brand.`brand-name` from item , brand where  item.`item-brand-id`=brand.`id` and item.`item-id`= ? ";
    public static String GET_SPEC_BY_ITEM_ID=" SELECT * FROM `specifications` WHERE `spec-item-id` = ? ";
    public static String GET_RECC_ITEMS_BY_ITEM_ID="SELECT * FROM `item` WHERE `item-recc-id`=?";
}
