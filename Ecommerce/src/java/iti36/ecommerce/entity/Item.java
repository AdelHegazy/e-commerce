/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.entity;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shika
 */
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer itemId;

    private String itemName;

    private int itemQuantity;

    private int itemPrice;

    private String itemType;

    private String accessorieType;

    private String itemDesc;

    private List<CartShop> cartShopList;

    private List<Item> itemList;

    private Item itemReccId;

    private Brand itemBrandId;

    private List<History> historyList;

    private Specifications specificationsList;

    private String imageBase64Binary;

    private String itemIdEncode;

    private InputStream imageStream;

    public Item() {
    }

    public Item(Integer itemId) {
        this.itemId = itemId;
    }

    public Item(Integer itemId, String itemName, int itemQuantity, int itemPrice, String itemType, String itemDesc) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemQuantity = itemQuantity;
        this.itemPrice = itemPrice;
        this.itemType = itemType;

        this.itemDesc = itemDesc;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getAccessorieType() {
        return accessorieType;
    }

    public void setAccessorieType(String accessorieType) {
        this.accessorieType = accessorieType;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public List<CartShop> getCartShopList() {
        return cartShopList;
    }

    public void setCartShopList(List<CartShop> cartShopList) {
        this.cartShopList = cartShopList;
    }

    public List<Item> getItemList() {
        if(itemList==null)
            itemList=new ArrayList<>();
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public Item getItemReccId() {
        return itemReccId;
    }

    public void setItemReccId(Item itemReccId) {
        this.itemReccId = itemReccId;
    }

    public Brand getItemBrandId() {
        return itemBrandId;
    }

    public void setItemBrandId(Brand itemBrandId) {
        this.itemBrandId = itemBrandId;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }

    public Specifications getSpecificationsList() {
        return specificationsList;
    }

    public void setSpecificationsList(Specifications specificationsList) {
        this.specificationsList = specificationsList;
    }

    public String getImageBase64Binary() {
        return imageBase64Binary;
    }

    public void setImageBase64Binary(String imageBase64Binary) {
        this.imageBase64Binary = imageBase64Binary;
    }

    public String getItemIdEncode() {
        return itemIdEncode;
    }

    public void setItemIdEncode(String itemIdEncode) {
        this.itemIdEncode = itemIdEncode;
    }
        public InputStream getImageStream() {
        return imageStream;
    }

    public void setImageStream(InputStream imageStream) {
        this.imageStream = imageStream;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemId != null ? itemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Item)) {
            return false;
        }
        Item other = (Item) object;
        if ((this.itemId == null && other.itemId != null) || (this.itemId != null && !this.itemId.equals(other.itemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.itemDesc+" "+this.itemName+" "+this.itemType+" "+this.accessorieType;
    }

}
