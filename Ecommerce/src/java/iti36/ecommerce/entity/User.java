/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.entity;

import java.io.Serializable;
import java.util.List;


/**
 *
 * @author Shika
 */

public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String job;

    private String country;

    private String city;
    
    private String street;
    
    private int balance;

    private List<CartShop> cartShopList;

    private List<History> historyList;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public User(Integer id, String firstName, String lastName, String email, String password, String job, String country,String Street ,String city, int balance) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.job = job;
        this.country = country;
        this.city = city;
        this.balance = balance;
        this.street = Street;
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public List<CartShop> getCartShopList() {
        return cartShopList;
    }

    public void setCartShopList(List<CartShop> cartShopList) {
        this.cartShopList = cartShopList;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }

    @Override
    public String toString() {
        return "iti36.ecommerce.bean.Users[ id=" + id + " ]";
    }
    
}
