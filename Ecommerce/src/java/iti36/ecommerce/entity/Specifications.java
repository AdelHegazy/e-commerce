/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.entity;

import java.io.Serializable;

/**
 *
 * @author Shika
 */
public class Specifications implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String networkTechnology;

    private String launchAnnounced;

    private String bodyDimensions;

    private String bodyWeight;

    private String bodyBuild;

    private String bodySim;

    private String displayType;

    private String displaySize;

    private String displayResolution;

    private String displayMultitouch;

    private String displayProtection;

    private String platformOs;

    private String platformChipset;

    private String platformCpu;

    private String platformGpu;

    private String memoryCardslot;

    private String memoryInternal;

    private String cameraPrimary;

    private String cameraFeatures;

    private String cameraVideo;

    private String cameraSecondary;

    private String soundAlerttypes;

    private String soundLoudspeaker;

    private String mmJack;

    private String commsWlan;

    private String commsBluetooth;

    private String commsGps;

    private String commsNfc;

    private String commsRadio;

    private String commsUsb;

    private String featuresSensors;

    private String featuresMessaging;

    private String featuresBrowser;

    private String featuresJava;

    private String battery;

    private String batteryStandby;

    private String batteryTalktime;

    private String miscColors;

    private Item specItemId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNetworkTechnology() {
        return networkTechnology;
    }

    public void setNetworkTechnology(String networkTechnology) {
        this.networkTechnology = networkTechnology;
    }

    public String getLaunchAnnounced() {
        return launchAnnounced;
    }

    public void setLaunchAnnounced(String launchAnnounced) {
        this.launchAnnounced = launchAnnounced;
    }

    public String getBodyDimensions() {
        return bodyDimensions;
    }

    public void setBodyDimensions(String bodyDimensions) {
        this.bodyDimensions = bodyDimensions;
    }

    public String getBodyWeight() {
        return bodyWeight;
    }

    public void setBodyWeight(String bodyWeight) {
        this.bodyWeight = bodyWeight;
    }

    public String getBodyBuild() {
        return bodyBuild;
    }

    public void setBodyBuild(String bODYBuild) {
        this.bodyBuild = bODYBuild;
    }

    public String getBodySim() {
        return bodySim;
    }

    public void setBodySim(String bodySim) {
        this.bodySim = bodySim;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public String getDisplaySize() {
        return displaySize;
    }

    public void setDisplaySize(String displaySize) {
        this.displaySize = displaySize;
    }

    public String getDisplayResolution() {
        return displayResolution;
    }

    public void setDisplayResolution(String displayResolution) {
        this.displayResolution = displayResolution;
    }

    public String getDisplayMultitouch() {
        return displayMultitouch;
    }

    public void setDisplayMultitouch(String displayMultitouch) {
        this.displayMultitouch = displayMultitouch;
    }

    public String getDisplayProtection() {
        return displayProtection;
    }

    public void setDisplayProtection(String displayProtection) {
        this.displayProtection = displayProtection;
    }

    public String getPlatformOs() {
        return platformOs;
    }

    public void setPlatformOs(String platformOs) {
        this.platformOs = platformOs;
    }

    public String getPlatformChipset() {
        return platformChipset;
    }

    public void setPlatformChipset(String platformChipset) {
        this.platformChipset = platformChipset;
    }

    public String getPlatformCpu() {
        return platformCpu;
    }

    public void setPlatformCpu(String platformCpu) {
        this.platformCpu = platformCpu;
    }

    public String getPlatformGpu() {
        return platformGpu;
    }

    public void setPlatformGpu(String platformGpu) {
        this.platformGpu = platformGpu;
    }

    public String getMemoryCardslot() {
        return memoryCardslot;
    }

    public void setMemoryCardslot(String memoryCardslot) {
        this.memoryCardslot = memoryCardslot;
    }

    public String getMemoryInternal() {
        return memoryInternal;
    }

    public void setMemoryInternal(String memoryInternal) {
        this.memoryInternal = memoryInternal;
    }

    public String getCameraPrimary() {
        return cameraPrimary;
    }

    public void setCameraPrimary(String cameraPrimary) {
        this.cameraPrimary = cameraPrimary;
    }

    public String getCameraFeatures() {
        return cameraFeatures;
    }

    public void setCameraFeatures(String cameraFeatures) {
        this.cameraFeatures = cameraFeatures;
    }

    public String getCameraVideo() {
        return cameraVideo;
    }

    public void setCameraVideo(String cameraVideo) {
        this.cameraVideo = cameraVideo;
    }

    public String getCameraSecondary() {
        return cameraSecondary;
    }

    public void setCameraSecondary(String cameraSecondary) {
        this.cameraSecondary = cameraSecondary;
    }

    public String getSoundAlerttypes() {
        return soundAlerttypes;
    }

    public void setSoundAlerttypes(String soundAlerttypes) {
        this.soundAlerttypes = soundAlerttypes;
    }

    public String getSoundLoudspeaker() {
        return soundLoudspeaker;
    }

    public void setSoundLoudspeaker(String soundLoudspeaker) {
        this.soundLoudspeaker = soundLoudspeaker;
    }

    public String getMmJack() {
        return mmJack;
    }

    public void setMmJack(String mmJack) {
        this.mmJack = mmJack;
    }

    public String getCommsWlan() {
        return commsWlan;
    }

    public void setCommsWlan(String commsWlan) {
        this.commsWlan = commsWlan;
    }

    public String getCommsBluetooth() {
        return commsBluetooth;
    }

    public void setCommsBluetooth(String commSBluetooth) {
        this.commsBluetooth = commSBluetooth;
    }

    public String getCommsGps() {
        return commsGps;
    }

    public void setCommsGps(String commsGps) {
        this.commsGps = commsGps;
    }

    public String getCommsNfc() {
        return commsNfc;
    }

    public void setCommsNfc(String commsNfc) {
        this.commsNfc = commsNfc;
    }

    public String getCommsRadio() {
        return commsRadio;
    }

    public void setCommsRadio(String commsRadio) {
        this.commsRadio = commsRadio;
    }

    public String getCommsUsb() {
        return commsUsb;
    }

    public void setCommsUsb(String commsUsb) {
        this.commsUsb = commsUsb;
    }

    public String getFeaturesSensors() {
        return featuresSensors;
    }

    public void setFeaturesSensors(String featuresSensors) {
        this.featuresSensors = featuresSensors;
    }

    public String getFeaturesMessaging() {
        return featuresMessaging;
    }

    public void setFeaturesMessaging(String featuresMessaging) {
        this.featuresMessaging = featuresMessaging;
    }

    public String getFeaturesBrowser() {
        return featuresBrowser;
    }

    public void setFeaturesBrowser(String featuresBrowser) {
        this.featuresBrowser = featuresBrowser;
    }

    public String getFeaturesJava() {
        return featuresJava;
    }

    public void setFeaturesJava(String featuresJava) {
        this.featuresJava = featuresJava;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getBatteryStandby() {
        return batteryStandby;
    }

    public void setBatteryStandby(String batteryStandby) {
        this.batteryStandby = batteryStandby;
    }

    public String getBatteryTalktime() {
        return batteryTalktime;
    }

    public void setBatteryTalktime(String batteryTalktime) {
        this.batteryTalktime = batteryTalktime;
    }

    public String getMiscColors() {
        return miscColors;
    }

    public void setMiscColors(String miscColors) {
        this.miscColors = miscColors;
    }

    public Item getSpecItemId() {
        return specItemId;
    }

    public void setSpecItemId(Item specItemId) {
        this.specItemId = specItemId;
    }

    public Specifications() {
    }

    public Specifications(Integer id) {
        this.id = id;
    }

   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Specifications)) {
            return false;
        }
        Specifications other = (Specifications) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.bodyWeight+" "+this.networkTechnology+" "+this.bodyBuild+" "+this.bodyDimensions;
    }

}
