/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.ecommerce.entity;

import java.io.Serializable;


/**
 *
 * @author Shika
 */

public class Credit implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer creditNumber;

    private int creditBalance;

    private boolean creditValid;

    public Credit() {
    }

    public Credit(Integer creditNumber) {
        this.creditNumber = creditNumber;
    }

    public Credit(Integer creditNumber, int creditBalance, boolean creditValid) {
        this.creditNumber = creditNumber;
        this.creditBalance = creditBalance;
        this.creditValid = creditValid;
    }

    public Integer getCreditNumber() {
        return creditNumber;
    }

    public void setCreditNumber(Integer creditNumber) {
        this.creditNumber = creditNumber;
    }

    public int getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(int creditBalance) {
        this.creditBalance = creditBalance;
    }

    public boolean getCreditValid() {
        return creditValid;
    }

    public void setCreditValid(boolean creditValid) {
        this.creditValid = creditValid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (creditNumber != null ? creditNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Credit)) {
            return false;
        }
        Credit other = (Credit) object;
        if ((this.creditNumber == null && other.creditNumber != null) || (this.creditNumber != null && !this.creditNumber.equals(other.creditNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "iti36.ecommerce.bean.Credit[ creditNumber=" + creditNumber + " ]";
    }
    
}
